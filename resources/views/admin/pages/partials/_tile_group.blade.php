<div class="flex flex-wrap w-full flex-auto">
    <div class="w-1/4 px-1">
        {!! Form::label("cms_tile_group[".$nodeContent->contentable->id."][tile_1]", "Tile #1", ["class" => "field-label"]) !!}
        {!! Form::select("cms_tile_group[".$nodeContent->contentable->id."][tile_1][]", $tiles->pluck("name", "id")->all(), $nodeContent->contentable->tile_1->pluck("id")->all(), ["class" => "selectize", "multiple", "placeholder" => "Select..."]) !!}
    </div>
    <div class="w-1/4 px-1">
        {!! Form::label("cms_tile_group[".$nodeContent->contentable->id."][tile_2]", "Tile #2", ["class" => "field-label"]) !!}
        {!! Form::select("cms_tile_group[".$nodeContent->contentable->id."][tile_2][]", $tiles->pluck("name", "id")->all(), $nodeContent->contentable->tile_2->pluck("id")->all(), ["class" => "selectize", "multiple",  "placeholder" => "Select..."]) !!}
    </div>
    <div class="w-1/4 px-1">
        {!! Form::label("cms_tile_group[".$nodeContent->contentable->id."][tile_3]", "Tile #3", ["class" => "field-label"]) !!}
        {!! Form::select("cms_tile_group[".$nodeContent->contentable->id."][tile_3][]", $tiles->pluck("name", "id")->all(), $nodeContent->contentable->tile_3->pluck("id")->all(), ["class" => "selectize", "multiple", "placeholder" => "Select..."]) !!}
    </div>
    <div class="w-1/4 px-1">
        {!! Form::label("cms_tile_group[".$nodeContent->contentable->id."][tile_4]", "Tile #4", ["class" => "field-label"]) !!}
        {!! Form::select("cms_tile_group[".$nodeContent->contentable->id."][tile_4][]", $tiles->pluck("name", "id")->all(), $nodeContent->contentable->tile_4->pluck("id")->all(), ["class" => "selectize", "multiple", "placeholder" => "Select..."]) !!}
    </div>
</div>
