@extends("admin.layouts.master")

@section('content')
{!! Form::open(['route' => ['admin.product_lists.update', $list->id], 'method' => 'PUT',  'enctype' => 'multipart/form-data']) !!}
    <div class="section-head">
        <div class="content">
            <h2 class="mb-2">Edit product list</h2>
        </div>
        {!! Form::submit('Save list', ['class' => 'button blue flex-shrink-0']) !!}
    </div>

    <div class="card-group mb-5">
        <div class="card-col w-full">
            <div class="card">
                <div class="flex flex-wrap">
                    <div class="flex flex-wrap w-full">
                        <fieldset class="mb-6 px-1 w-full">
                            @if($list->getFirstMediaUrl("background"))
                                {!! HTML::image($list->getFirstMediaUrl("background"), $list->name, ["class" => "mx-auto block", "style" => "max-height: 200px;"]) !!}
                            @endif
                            {!! Form::label("background", ($list->getFirstMediaUrl("background") ? "Update" : "Add")." Background", ["class" => "field-label mx-auto block", "style" => "max-width: 200px;"]) !!}
                            {!! Form::file("background", ["class" => "block mx-auto"]) !!}
                        </fieldset>
                        <fieldset class="mb-6 px-1 w-1/2">
                            {!! Form::label('name', 'Name', ['class' => 'field-label']) !!}
                            {!! Form::text('name', $list->name, ['class' => 'field-input']) !!}
                            <span class="field-error">{!! $errors->first('name') !!}</span>
                        </fieldset>
                        <fieldset class="mb-6 px-1 w-1/2">
                            {!! Form::label('featured', 'Featured', ['class' => 'field-label']) !!}
                            {!! Form::checkbox("featured", 1, $list->featured, ["class" => "ml-4"]) !!}
                            <span class="field-error">{!! $errors->first('featured') !!}</span>
                        </fieldset>
                        <fieldset class="mb-6 px-1 w-full">
                            {!! Form::label('description', 'Description', ['class' => 'field-label']) !!}
                            {!! Form::textarea('description', $list->description, ['class' => 'field-input']) !!}
                        </fieldset>
                        <fieldset class="mb-6 px-1 w-1/2">
                            {!! Form::label("button_text", "Button Text", ["class" => "field-label"]) !!}
                            {!! Form::text('button_text', $list->button_text, ['class' => 'field-input']) !!}
                        </fieldset>
                        <fieldset class="mb-6 px-1 w-1/2">
                            {!! Form::label("button_link", "Button Link", ["class" => "field-label"]) !!}
                            {!! Form::text('button_link', $list->button_link, ['class' => 'field-input']) !!}
                        </fieldset>
                        <fieldset class="mb-6 px-1 w-1/2">
                            {!! Form::label('colour_select', "Select Button Colour", ['class' => 'field-label']) !!}
                            {!! Form::select('colour_select', $colours, $list->button_colour, ['class' => 'selectize', 'placeholder' => 'Select...', 'id' => 'colour_select']) !!}
                            <span class="field-error">{!! $errors->first('colour') !!}
                        </fieldset>
                        <fieldset class="mb-6 px-1 w-1/2">
                            {!! Form::label('colour_input', "...or input a custom value", ["class" => "field-label"]) !!}
                            {!! Form::text("colour_input", !in_array($list->button_colour, array_keys($colours)) ? $list->button_colour : null, ["class" => "field-input", 'id' => 'colour_input']) !!}
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card-group mb-5">
        <div class="card-col w-full">
            <div class="card">
                <div class="flex flex-wrap">
                    <div class="flex flex-wrap w-full">
                        <fieldset class="mb-6 px-1 w-full">
                            {!! Form::label("products", "Products", ["class" => "field-label"]) !!}
                            {!! Form::select("products[]", $products->pluck("name", "id")->all(), $list->products->pluck("id")->all(), ["class" => "selectize", "multiple", "placeholder" => "Select..."]) !!}
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
{!! Form::close() !!}
@stop

@section('templates')
@stop

@section('scripts')
    <script>
        $("#colour_select").on("change", function() {
            if ($(this).val().length)
                $("#colour_input").val("");
        });

        $("#colour_input").on("change", function() {
            if ($(this).val().length)
                $("#colour_select")[0].selectize.clear();
        });
    </script>
@stop
