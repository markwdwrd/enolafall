@extends('admin.layouts.master')

@section('content')
<a href="{!! url('admin/sliders') !!}" title="Sliders" class="block text-xs font-semibold text-gray-600 hover:text-blue-500 -mb-2"><i class="fa fa-caret-left mr-2"></i>Back to sliders</a>
{!! Form::model($slider, ['route' => ['admin.sliders.update', $slider->id], 'method' => 'PUT',  'enctype' => 'multipart/form-data']) !!}
    <div class="section-head">
        <div class="content">
            <h2 class="mb-2">Edit slider</h2>
        </div>
        {!! Form::submit('Save slider', ['class' => 'button blue flex-shrink-0']) !!}
    </div>

    <div class="w-full mt-8 mb-4 px-2">
        <h3>Slider details</h3>
    </div>
    <div class="card-group">
        <div class="card-col w-full">
            <div class="card">
                <fieldset class="mb-2 px-1">
                    {!! Form::label('name', 'Slider Name', ['class' => 'field-label']) !!}
                    {!! Form::text('name', $slider->name, ['id' => 'name',  'class' => 'field-input']) !!}
                    <span class="field-error">{!! $errors->first('name') !!}</span>
                </fieldset>
                <fieldset class="mb-2 px-1">
                    {!! Form::label('slug', 'Slug', ['class' => 'field-label']) !!}
                    {!! Form::text('slug', $slider->slug, ['id' => 'slug', 'class' => 'field-input']) !!}
                    <span class="field-error">{!! $errors->first('slug') !!}</span>
                </fieldset>
            </div>
        </div>
    </div>

    <div class="section-head">
        <div class="content">
            <h3>Slider images</h3>
        </div>
        <input type="file" name="images" data-fileuploader-files="{{ json_encode($media) }}">
    </div>
    <div id="slider-images" class="card-group">
    </div>
@stop

@section('templates')
    <script type="html/template" id="slider-image">
        <div class="fileuploader-item card-col w-full sm:w-1/2" data-id="${data.id}">
            <div class="card image">
                <a class="text-red-500 fileuploader-action-remove" mediaid="${data.id}"><i class="fa fa-trash"></i></a>
                <figure class="wide bg-gray-100">
                    ${image}
                </figure>
                <div class="progress-bar2">${progressBar}<span></span></div>
                <fieldset class="mb-2 px-1">
                    <label class="field-label">Title</label>
                    {!! Form::text('media_name[${data.id}]', '${name}', ['class' => 'field-input']) !!}
                </fieldset>
                <fieldset class="mb-2 px-1">
                    <label class="field-label">Headline</label>
                    {!! Form::text('media_headline[${data.id}]', '${data.headline}', ['class' => 'field-input']) !!}
                </fieldset>
                <fieldset class="mb-2 px-1">
                    <label class="field-label">Subhead</label>
                    {!! Form::text('media_subhead[${data.id}]', '${data.subhead}', ['class' => 'field-input']) !!}
                </fieldset>
                <fieldset class="mb-2 px-1">
                    <label class="field-label">Description</label>
                    {!! Form::textarea('media_description[${data.id}]', '${data.description}', ['class' => 'field-input h-32']) !!}
                </fieldset>
                {{--<fieldset class="mb-2 px-1">
                    <label class="field-label">Slide URL</label>
                    {!! Form::text('media_slide_url[${data.id}]', '${data.slide_url}', ['class' => 'field-input']) !!}
                </fieldset>
                <div class="flex">
                    <fieldset class="mb-2 px-1 w-1/2">
                        <label class="field-label">Price Text</label>
                        {!! Form::text('media_price_text[${data.id}]', '${data.price_text}', ['class' => 'field-input']) !!}
                    </fieldset>
                    <fieldset class="mb-2 px-1 w-1/2">
                        <label class="field-label">Button Text</label>
                        {!! Form::text('media_button_text[${data.id}]', '${data.button_text}', ['class' => 'field-input']) !!}
                    </fieldset>
                </div>
                <fieldset class="mb-2 px-1">
                    <label class="field-label">Button URL</label>
                    {!! Form::text('media_button_url[${data.id}]', '${data.button_url}', ['class' => 'field-input']) !!}
                </fieldset>--}}
                <fieldset class="mb-2 px-1">
                    <label class="field-label">Box Position</label>
                    <select name="media_position[${data.id}]" class="selectize" data-sort="[]">
                        @foreach($positions as $key => $value)
                            <option value="{!! $key !!}" ${data.position == '{{ $key }}' ? 'selected' : ''}>{{ $value }}</option>
                        @endforeach
                    </select>
                </fieldset>
            </div>
        </div>
    </script>
@stop

@section('scripts')
    <script type="text/javascript">
        $('input[name="images"]').fileuploader({
            theme: null,
            addMore: true,
            extensions: ['jpg', 'jpeg', 'png', 'gif', 'mp4'],
            fileMaxSize: 10,
            changeInput: '<button class="button blue flex-shrink-0">Add slide</button>',
            thumbnails: {
                canvasImage: false,
                box: '<div class="fileuploader-items-list card-group w-full"></div>',
                boxAppendTo: $('#slider-images'),
                item: function(item) {
                    return $('#slider-image').html();
                },
                item2: function(item) {
                    return $('#slider-image').html();
                },
            },
            upload: {
                url: "{!! url('admin/sliders/' . $slider->id . '/slides') !!}",
                data: null,
                type: 'POST',
                enctype: 'multipart/form-data',
                start: true,
                onSuccess: function(data, item, listEl, parentEl, newInputEl, inputEl, textStatus, jqXHR) {

                    var id = data.id;
                    item.html.find('input, textarea, select').each(function(i, elem) {
                        var name = $(this).attr('name');
                        $(this).attr('name', name.slice(0, -1) + id + name.slice(-1));
                    });

                    item.html.find('.fileuploader-action-remove').addClass('fileuploader-action-success').attr('mediaid', id);

                    setTimeout(function() {
                        item.html.find('.progress-bar2').fadeOut(400);
                    }, 400);

                    $('.colourpicker').spectrum({
                        preferredFormat: "rgb",
                        showAlpha: true,
                        showInput: true
                    });

                    initializeSelectize();
                },

                onError: function(item, listEl, parentEl, newInputEl, inputEl, jqXHR, textStatus, errorThrown) {
                    console.error('ERROR', errorThrown);
                    var progressBar = item.html.find('.progress-bar2');

                    if(progressBar.length > 0) {
                        progressBar.find('span').html(0 + "%");
                        progressBar.find('.fileuploader-progressbar .bar').width(0 + "%");
                        item.html.find('.progress-bar2').fadeOut(400);
                    }

                    item.upload.status != 'cancelled' && item.html.find('.fileuploader-action-retry').length == 0 ? item.html.find('.column-actions').prepend(
                        '<a class="fileuploader-action fileuploader-action-retry" title="Retry"><i></i></a>'
                    ) : null;
                },

                // Callback fired while upload the file
                // we will set by default the progressbar width and percentage
                /* data = {
                    loaded: ...,
                    loadedInFormat: ...,
                    total: ...,
                    totalInFormat: ...,
                    percentage: ...,
                    secondsElapsed: ...,
                    secondsElapsedInFormat:...,
                    bytesPerSecond: ...,
                    bytesPerSecondInFormat: ...,
                    remainingBytes: ...,
                    remainingBytesInFormat: ...,
                    secondsRemaining: ...,
                    secondsRemainingInFormat: ...
                } */
                onProgress: function(data, item, listEl, parentEl, newInputEl, inputEl) {
                    var progressBar = item.html.find('.progress-bar2');

                    if(progressBar.length > 0) {
                        progressBar.show();
                        progressBar.find('span').html(data.percentage + "%");
                        progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
                    }
                },
                onComplete: function(listEl, parentEl, newInputEl, inputEl, jqXHR, textStatus) {
                }
            },
            editor: {
                cropper: {
                    showGrid: true
                }
            },

            // Callback fired after deleting a file
            // by returning false, you can prevent a file from removing
            onRemove: function(item, listEl, parentEl, newInputEl, inputEl) {
                var mediaid = item.html.find('.fileuploader-action-remove').attr('mediaid');

                $.ajax({
                    url: '/admin/media/' + mediaid,
                    type: 'DELETE'
                }).done(function(response) {
                    return true;
                }).fail(function(error) {
                    // TODO - show errors on the page
                    console.log(error);
                    return false;
                }).always(function() {
                }, 'json');
            },
        });

        function initializeSelectize() {
            $('select.selectize').each(function () {
                var create = false;
                var delimiter = ',';
                var persist = false;
                var openOnFocus = true;
                var sortField = 'value';

                // TODO Change parameters to data-attributes
                if ($(this).hasClass('create')) {
                    create = true;
                }

                if ($(this).hasClass('sort-index')) {
                    sortField = [{
                        field: 'index',
                        direction: 'desc'
                        }, {
                        field: '$score'
                    }];
                };

                if ($(this).hasClass('tag')) {
                    openOnFocus = false;
                    var modelid = $(this).data('model-id');
                    create = function (input) {
                        var url = 'tag/create';
                        var slug = input.toLowerCase().replace(/ /g, '-').replace(/[-]+/g, '-').replace(/[^\w-]+/g, '');
                        $.ajax({
                            'type': 'get',
                            'url': url,
                            'async': false,
                            'data': {
                                name: input,
                                slug: slug,
                                modelid: modelid,
                            }
                        }).done(function (response) {
                            if (response.status === 201) {
                                console.log(response);
                                id = response.id;
                            } else {
                                console.log('ajaxerror', response);
                            }
                        }).fail(function (response) {
                            console.log('ajaxerror', response);
                        }).always(function (response) {
                            console.log('ajaxalways', response);
                        });
                        return {
                            value: id,
                            text: input
                        };
                    }
                }

                $(this).selectize({
                    delimiter: delimiter,
                    persist: persist,
                    create: create,
                    sortField: 'value',
                    openOnFocus: openOnFocus
                });
            });
        };

        $(document).ready(function() {
            $('.card-group').sortable({
                cursor: 'move',
                placeholder: 'placeholder',
                forcePlaceholderSize: true,
                cancel: '.field-input',
                stop: function(event, ui) {
                    i = ui.item;
                    s = i.parent();
                    p = s.parent();
                    sortOrder($(this), i, p, s);
                }
            });
        });

        function sortOrder(e, i, p, s) {
            var order = [];
            $(e).children().each(function() {
                order.push($(this).data('id'));
            });

            $.ajax({
                type: 'POST',
                url: '/admin/sliders/{{$slider->id}}/slides/sort',
                data: {
                    'order': order
                }
            }).done(function(response) {
                console.log(response);
            }).fail(function(response) {
                console.log('ajaxerror', response);
            });

            return false;
        }

    </script>
@stop
