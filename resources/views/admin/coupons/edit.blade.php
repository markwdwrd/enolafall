@extends("admin.layouts.master")

@section('content')
{!! Form::open(['route' => ['admin.coupons.update', $coupon->id], 'method' => 'PUT',  'enctype' => 'multipart/form-data']) !!}
    <div class="section-head">
        <div class="content">
            <h2 class="mb-2">Edit coupon</h2>
        </div>
        {!! Form::submit('Save coupon', ['class' => 'button blue flex-shrink-0']) !!}
    </div>

    <div class="card-group mb-5">
        <div class="card-col w-full">
            <div class="card">
                <div class="flex flex-wrap">
                    <div class="flex flex-wrap w-full">
                        <fieldset class="mb-6 px-1 w-full">
                            {!! Form::label('code', 'Code', ['class' => 'field-label']) !!}
                            {!! Form::text('code', $coupon->code, ['class' => 'field-input']) !!}
                            <span class="field-error">{!! $errors->first('code') !!}</span>
                        </fieldset>
                        <fieldset class="mb-6 px-1 w-1/2">
                            {!! Form::label('discount', 'Discount', ['class' => 'field-label']) !!}
                            {!! Form::text('discount', $coupon->discount, ['class' => 'field-input']) !!}
                            <span class="field-error">{!! $errors->first('discount') !!}</span>
                        </fieldset>
                        <fieldset class="mb-6 px-1 w-1/2">
                            {!! Form::label("sign", "Sign", ["class" => "field-label"]) !!}
                            {!! Form::select("sign", ["$" => "$", "%" => "%"], $coupon->sign, ["class" => "selectize", "placeholder" => "Select..."]) !!}
                            <span class="field-error">{!! $errors->first('sign') !!}</span>
                        </fieldset>
                        <fieldset class="mb-6 px-1 w-1/2">
                            {!! Form::label('date_start', 'Active From', ['class' => 'field-label']) !!}
                            {!! Form::text('date_start', $coupon->date_start, ['class' => 'field-input flatpickr',
                                'data-date-format' => 'Y-m-d',
                                'data-alt-input' => 'true',
                                'data-alt-format' => 'l F J, Y']) !!}
                            <span class="field-error">{!! $errors->first('date_start') !!}</span>
                        </fieldset>
                        <fieldset class="mb-6 px-1 w-1/2">
                            {!! Form::label('date_expires', 'Active To', ['class' => 'field-label']) !!}
                            {!! Form::text('date_expires', $coupon->date_expires, ['class' => 'field-input flatpickr',
                                'data-date-format' => 'Y-m-d',
                                'data-alt-input' => 'true',
                                'data-alt-format' => 'l F J, Y']) !!}
                            <span class="field-error">{!! $errors->first('date_expires') !!}</span>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>

{!! Form::close() !!}
@stop

@section('scripts')
@stop
