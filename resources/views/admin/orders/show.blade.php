@extends("admin.layouts.master")

@section('content')
    <div class="section-head">
        <div class="content">
            <h2 class="mb-2">View order</h2>
        </div>
    </div>

    <div class="card-group mb-5">
        <div class="card-col w-full">
            <div class="card">
                <div class="flex flex-wrap">
                    <div class="flex flex-wrap w-full">
                        <fieldset class="mb-6 px-1 w-1/2">
                            {!! Form::label('user', 'User', ['class' => 'field-label']) !!}
                            @if($order->user)
                                <p><a href="/admin/users/{{$order->user_id}}/edit" class="text-blue-500 ml-2">{{$order->user->full_name}}</a></p>
                            @else
                                <p class="ml-2">Guest checkout with email address {{$order->email}}</p>
                            @endif
                        </fieldset>

                        <fieldset class="mb-6 px-1 w-1/2">
                            {!! Form::label('completed_at', 'Completed at', ['class' => 'field-label']) !!}
                            @if($order->completed_at != null)
                                <p class="ml-2">{{$order->completed_at->format("d/m/Y H:i:s")}}</p>
                            @else
                                <p class="ml-2">Order not completed</p>
                            @endif
                        </fieldset>

                        <fieldset class="mb-6 px-1 w-1/2">
                            {!! Form::label('payment_method', 'Payment method', ['class' => 'field-label']) !!}
                            <p class="ml-2">{{$order->payment_method}}</p>
                        </fieldset>

                        <fieldset class="mb-6 px-1 w-1/2">
                            {!! Form::label('payment_reference', 'Payment reference', ['class' => 'field-label']) !!}
                            <p class="ml-2">{{$order->payment_reference}}</p>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-col w-full">
            <div class="card">
                <div class="flex flex-wrap">
                    @if($order->shippingAddress)
                        <div class="w-1/2">
                            <h2>Shipping Address</h2>

                            <span><b>Name</b>: {{$order->shippingAddress->name}}</span><br>
                            <span><b>Company</b>: {{$order->shippingAddress->company}}</span><br>
                            <span><b>Address Line 1</b>: {{$order->shippingAddress->address1}}</span><br>
                            <span><b>Address Line 2</b>: {{$order->shippingAddress->address2}}</span><br>
                            <span><b>Suburb</b>: {{$order->shippingAddress->suburb}}</span><br>
                            <span><b>State</b>: {{$order->shippingAddress->state}}</span><br>
                            <span><b>Postcode</b>: {{$order->shippingAddress->postcode}}</span>
                        </div>
                    @endif

                    @if($order->billingAddress)
                        <div class="w-1/2">
                            <h2>Billing Address</h2>

                            <span><b>Name</b>: {{$order->billingAddress->name}}</span><br>
                            <span><b>Company</b>: {{$order->billingAddress->company}}</span><br>
                            <span><b>Address Line 1</b>: {{$order->billingAddress->address1}}</span><br>
                            <span><b>Address Line 2</b>: {{$order->billingAddress->address2}}</span><br>
                            <span><b>Suburb</b>: {{$order->billingAddress->suburb}}</span><br>
                            <span><b>State</b>: {{$order->billingAddress->state}}</span><br>
                            <span><b>Postcode</b>: {{$order->billingAddress->postcode}}</span>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="card-col w-full">
            <div class="card">
                <div class="flex flex-wrap">
                    <div class="flex flex-wrap w-3/5">
                        <h2>Products Ordered</h2>
                        <table class="w-full">
                            <thead>
                                <tr>
                                    <th class="border-gray-400 border-solid border text-sm text-gray-800 font-medium py-2 px-4 bg-gray-100">Name</th>
                                    <th class="border-gray-400 border-solid border text-sm text-gray-800 font-medium py-2 px-4 bg-gray-100">Quantity</th>
                                    <th class="border-gray-400 border-solid border text-sm text-gray-800 font-medium py-2 px-4 bg-gray-100"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($order->items as $item)
                                    <tr>
                                        <td class="border-gray-400 border-solid border py-2 px-4 text-gray-700 text-sm">{{$item->variant->name}}</td>
                                        <td class="border-gray-400 border-solid border py-2 px-4 text-gray-700 text-sm">{{$item->quantity}}</td>
                                        <td class="border-gray-400 border-solid border py-2 px-4 text-gray-700 text-sm"><a href="/admin/variants/{{$item->variant_id}}/edit" class="fa fa-edit"></i></a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="w-2/5 pl-5">
                        <h2>Order Totals</h2>
                        <table class="w-full">
                            <tr>
                                <td class="border-gray-400 border-solid border text-sm text-gray-800 font-medium py-2 px-4 bg-gray-100">Items</td>
                                <td class="border-gray-400 border-solid border py-2 px-4 text-gray-700 text-sm">{{$order->cart_count}}</td>
                            </tr>
                            <tr>
                                <td class="border-gray-400 border-solid border text-sm text-gray-800 font-medium py-2 px-4 bg-gray-100">Subtotal</td>
                                <td class="border-gray-400 border-solid border py-2 px-4 text-gray-700 text-sm">${{$order->item_total}}</td>
                            </tr>
                            @if($order->coupon)
                                <tr>
                                    <td class="border-gray-400 border-solid border text-sm text-gray-800 font-medium py-2 px-4 bg-gray-100">Coupon</td>
                                    <td class="border-gray-400 border-solid border py-2 px-4 text-gray-700 text-sm">- ${{$order->coupon_discount}}</td>
                                </tr>
                            @endif
                            <tr>
                                <td class="border-gray-400 border-solid border text-sm text-gray-800 font-medium py-2 px-4 bg-gray-100">Total</td>
                                <td class="border-gray-400 border-solid border py-2 px-4 text-gray-700 text-sm">${{$order->order_total}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
@stop
