@extends("admin.layouts.master")
@section('content')
<div class="section-head">
    <div class="content">
        <h2 class="mb-2">Orders</h2>
        <div class="text-sm md:text-base">
            <p>Manage Orders</p>
        </div>
    </div>
</div>

<div class="card-group">
    <div class="card-col w-full">
        <div class="card">
            <div class="flex flex-wrap">
                {!! Form::open(['route' => 'admin.orders.index', 'class' => 'w-full flex flex-wrap', 'method' => 'GET']) !!}
                    <div class="w-1/4 px-2">
                        {!! Form::label('email', 'Email', ['class' => 'field-label']) !!}
                        {!! Form::text("email", !empty(request()->email) ? request()->email : null, ['id' => 'email', 'class' => 'field-input small flex-auto', 'placeholder' => 'Search by Email']) !!}
                    </div>

                    <div class="w-1/4 px-2">
                        {!! Form::label('completed', 'Completed After', ['class' => 'field-label']) !!}
                        {!! Form::text("completed", !empty(request()->completed) ? request()->completed : null, ['id' => 'completed', 'class' => 'field-input small flex-auto flatpickr', 'placeholder' => 'Completed After...',
                            'data-enable-time' => 'true',
                                'data-date-format' => 'Y-m-d H:i:S',
                                'data-alt-input' => 'true',
                                'data-alt-format' => 'l F J, Y - h:i K']) !!}
                    </div>

                    <div class="w-1/4 px-2">
                        {!! Form::submit('Search', ['class' => 'button blue light block mt-10 leading-none']) !!}
                    </div>
                {!! Form::close() !!}
            </div>

            <div class="divider my-10"></div>

            @if (count($orders) > 0)
                {!! $orders->links() !!}

                <div class="table-container overflow-scroll">
                    <table class="w-full" style="min-width: 900px;">
                        <thead>
                            <tr>
                                <th class="border-gray-400 border-solid border text-sm text-gray-800 font-medium py-2 px-4 bg-gray-100">ID</th>
                                <th class="border-gray-400 border-solid border text-sm text-gray-800 font-medium py-2 px-4 bg-gray-100">Email</th>
                                <th class="border-gray-400 border-solid border text-sm text-gray-800 font-medium py-2 px-4 bg-gray-100">Completed At</th>
                                <th class="border-gray-400 border-solid border text-sm text-gray-800 font-medium py-2 px-4 bg-gray-100">Status</th>
                                <th class="border-gray-400 border-solid border text-sm text-gray-800 font-medium py-2 px-4 bg-gray-100"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td class="border-gray-400 border-solid border py-2 px-4 text-gray-700 text-sm">{{$order->id}}</td>
                                    <td class="border-gray-400 border-solid border py-2 px-4 text-gray-700 text-sm">{{$order->email}}</td>
                                    <td class="border-gray-400 border-solid border py-2 px-4 text-gray-700 text-sm">{{$order->completed_at != null ? $order->completed_at->format("d/m/Y H:i:s") : "N/A"}}</td>
                                    <td class="border-gray-400 border-solid border py-2 px-4 text-gray-700 text-sm">{{$order->state}}</td>
                                    <td class="border-gray-400 border-solid border py-2 px-4 text-gray-700 text-sm"><a href="{!! url('/admin/orders/'.$order->id.'/show') !!}" class="text-green-500 mr-2" title="View order"><i class="fa fa-eye"></i></a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                {!! $orders->links() !!}
            @else
                <span class="list-empty">There are no orders to display.</span>
            @endif
        </div>
    </div>
</div>
@stop

@section('templates')
@stop

@section('scripts')
@stop
