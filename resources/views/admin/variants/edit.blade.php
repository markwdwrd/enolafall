@extends('admin.layouts.master')

@section('content')
<a href="{!! url('admin/products/'  . $variant->product->id . '/edit') !!}" title="Sliders" class="block text-xs font-semibold text-gray-600 hover:text-blue-500 -mb-2"><i class="fa fa-caret-left mr-2"></i>Back to product</a>
{!! Form::model($variant, ['route' => ['admin.variants.update', $variant->id], 'method' => 'PUT',  'enctype' => 'multipart/form-data']) !!}
    <div class="section-head">
        <div class="content">
            <h2 class="mb-2">Edit Variant</h2>
        </div>
        {!! Form::submit('Save variant', ['class' => 'button blue flex-shrink-0']) !!}
    </div>

    <div class="w-full mt-8 mb-4 px-2">
        <h3>Variant details</h3>
    </div>
    <div class="card-group">
        <div class="card-col w-full md:w-1/2">
            <div class="card">
                <div class="flex flex-wrap mb-6">
                    <fieldset class="w-full pr-1">
                        {!! Form::label('name', 'Name', ['class' => 'field-label']) !!}
                        {!! Form::text('name', null, ['class' => 'field-input']) !!}
                        <span class="field-error">{!! $errors->first('name') !!}</span>
                    </fieldset>
                    <fieldset class="w-full pr-1">
                        {!! Form::label('slug', 'Slug', ['class' => 'field-label']) !!}
                        {!! Form::text('slug', null, ['class' => 'field-input']) !!}
                        <span class="field-error">{!! $errors->first('slug') !!}</span>
                        <span class="field-tip">The slug is used in the page URL and must be unique, lowercase and contain only letters, numbers and hyphens. Changing a page's slug may break links to it and is not recommended.</span>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="card-col w-full md:w-1/2">
            <div class="card">
                <div class="flex flex-wrap mb-6">
                    <fieldset class="w-full pr-1">
                        <div class="switch">
                            <label class="switch-label">
                                Variant active
                                {!! Form::checkbox('is_active', true, null, ['class' => 'switch-input']) !!}
                            </label>
                        </div>
                    </fieldset>
                </div>
                <div class="flex flex-wrap mb-2">
                    <fieldset class="w-full mb-2 px-1">
                    {!! Form::label('description', 'Description', ['class' => 'field-label']) !!}
                    {!! Form::textarea('description', null, ['class' => 'field-input h-32', 'placeholder' => 'Description' ]) !!}
                    </fieldset>
                </div>
            </div>
        </div>
    </div>

    <div class="w-full mt-8 mb-4 px-2">
        <h3>Variant values</h3>
    </div>
    <div class="card-group">
        <div class="card-col w-full">
            <div class="card">
                <div class="flex flex-wrap mb-2">
                    <fieldset class="w-full md:w-1/3 pr-1">
                        {!! Form::label('sku', 'SKU', ['class' => 'field-label']) !!}
                        {!! Form::text('sku', null, ['class' => 'field-input']) !!}
                        <span class="field-error">{!! $errors->first('sku') !!}</span>
                    </fieldset>
                    <fieldset class="w-full md:w-1/3 pr-1">
                        {!! Form::label('price', 'Price ($)', ['class' => 'field-label']) !!}
                        {!! Form::number('price', null, ['class' => 'field-input', 'step' => 'any']) !!}
                        <span class="field-error">{!! $errors->first('price') !!}</span>
                        <span class="field-tip">Default price.</span>
                    </fieldset>
                    <fieldset class="w-full md:w-1/3 pr-1">
                        {!! Form::label('sale_price', 'Sale Price ($)', ['class' => 'field-label']) !!}
                        {!! Form::number('sale_price', null, ['class' => 'field-input', 'step' => 'any']) !!}
                        <span class="field-error">{!! $errors->first('sale_price') !!}</span>
                        <span class="field-tip">Enter a lower price to override defualt price.</span>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>

    @if(count($taxonomies))
    <div class="w-full mt-8 mb-4 px-2">
        <h3>Variant taxonomy</h3>
    </div>
    <div class="card-group">
        <div class="card">
            <div class="flex flex-wrap mb-2">
                @foreach($taxonomies as $taxonomy)
                <div class='flex-auto mr-2 mb-2 w-1/3'>
                    {!! Form::label($taxonomy->slug, $taxonomy->name, ['class' => 'field-label']) !!}
                    {!! Form::select('taxon_ids[]',
                        $taxonomy->taxons->pluck('name', 'id')->all(),
                        $variant->taxons->pluck('id'),
                        [
                            'id' => $taxonomy->slug,
                            'class' => 'selectize sort-index create',
                            'placeholder' => 'Select ' . $taxonomy->name,
                            'data-create-url' => url('admin/taxonomies/' .  $taxonomy->id . '/taxon' ),
                        ]
                    ) !!}
                </div>
                @endforeach
            </div>
        </div>
    </div>
    @endif

{!! Form::close() !!}
@stop

@section('scripts')
    <script type="text/javascript">
    </script>
@stop
