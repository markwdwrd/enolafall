@extends('layouts.master')

@section('content')
    @include('account.partials._links')

    <h2>Addresses</h2>
    {!! Form::model($user, ['route' => 'account.addresses', 'method' => 'POST']) !!}
        <h3>Shipping Address</h3>
        <div id="shipping-address">
            @include('account.partials._address', ['prefix' => 'shipping'])
        </div>

        <h3>Billing Address</h3>
        <fieldset>
            {!! Form::checkbox('shipping_billing_same', null, true, ['id' => 'shipping_billing_same']) !!}
            {!! Form::label('shipping_billing_same', 'Same as shipping') !!}
        </fieldset>
        <div id="billing-address">
            @include('account.partials._address', ['prefix' => 'billing'])
        </div>

        {!! Form::button('Update', ['type' => 'submit']) !!}
    {!! Form::close() !!}
@stop

@section('inline-scripts')
@stop
