@extends('layouts.master')


@section('content')
    @include('account.partials._links')

    {!! Form::model($user, ['route' => 'account.details', 'method' => 'POST']) !!}
        <fieldset>
            {!! Form::label('first_name', 'First Name') !!}
            {!! Form::text('first_name', null) !!}
            <span class="error">{!! $errors->first('first_name') !!}</span>
        </fieldset>

        <fieldset>
            {!! Form::label('surname', 'Surname') !!}
            {!! Form::text('surname', null) !!}
            <span class="error">{!! $errors->first('surname') !!}</span>
        </fieldset>

        <fieldset>
            {!! Form::label('email', 'Email') !!}
            {!! Form::text('email', null) !!}
            <span class="error">{!! $errors->first('email') !!}</span>
        </fieldset>

        {!! Form::button('Update', ['type' => 'submit']) !!}
    {!! Form::close() !!}
@stop
