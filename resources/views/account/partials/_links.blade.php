
    <ul>
        <li>
            <a href="{!! url('/account/details') !!}">Account Details</a>
        </li>
        <li>
            <a href="{!! url('/account/orders') !!}">Order History</a>
        </li>
        <li>
            <a href="{!! url('/account/addresses') !!}">Addresses</a>
        </li>
        <li>
            <a href="{!! url('/account/password') !!}">Change Password</a>
        </li>
    </ul>
