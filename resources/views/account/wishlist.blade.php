@extends("layouts.utility")

{{-- @section('breadcrumb')
<div class="breadcrumb">
    <span class="breadcrumb__parent"><a href="{{ env('APP_URL') }}">Home</a></span>
    <img class="breadcrumb__divider" src="/img/bc_div.svg" alt="Breadcrumb Divider">
    <span class="breadcrumb__parent">Account</span>
    <img class="breadcrumb__divider" src="/img/bc_div.svg" alt="Breadcrumb Divider">
    <span class="breadcrumb__active">Wishlist</span>
</div>
@endsection --}}

@section("content")
<div class="account">
    <div class="account__sidebar">
        @include("account.partials._links")
    </div>
    <div class="account__body">
        <h2>Wishlist</h2>

        @if(auth()->user()->wishlist->count() > 0)
            <div class="products__list">
                @foreach(auth()->user()->wishlist as $item)
                    @include("partials._product", ["product" => $item->product])
                @endforeach
            </div>
        @else
            <p>You haven't added any items to your wishlist yet!</p>
        @endif
    </div>
</div>
@stop
