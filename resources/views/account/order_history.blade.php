@extends("layouts.utility")

{{-- @section('breadcrumb')
<div class="breadcrumb">
    <span class="breadcrumb__parent"><a href="{{ env('APP_URL') }}">Home</a></span>
    <img class="breadcrumb__divider" src="/img/bc_div.svg" alt="Breadcrumb Divider">
    <span class="breadcrumb__parent">Account</span>
    <img class="breadcrumb__divider" src="/img/bc_div.svg" alt="Breadcrumb Divider">
    <span class="breadcrumb__active">Order History</span>
</div>
@endsection --}}

@section("content")
<div class="account">
    <div class="account__sidebar">
        @include("account.partials._links")
    </div>
    <div class="account__body">
        <h2>Order History</h2>
        @if($orders->count() < 1)
            <p>You haven't placed any orders yet.</p>
        @else
            {!! $orders->links() !!}

            <table>
                <thead>
                    <th><b>Reference No.</b></th>
                    <th><b>Completed At</b></th>
                    <th><b>State</b></th>
                    <th></th>
                </thead>
                @foreach($orders as $order)
                    <tr>
                        <td><a href="/account/orders/{{$order->token}}/show">{{$order->id}}</a></td>
                        <td>{{$order->completed_at != null ? $order->completed_at->format("d/m/Y H:i:s") : ""}}</td>
                        <td>{{ucfirst($order->state)}}</td>
                        <td><a href="/account/orders/{{$order->token}}/show" class="button">View</a></td>
                    </tr>
                @endforeach
            </table>

            {!! $orders->links() !!}
        @endif
    </div>
</div>
@stop
