@extends("layouts.utility")

{{-- @section('breadcrumb')
<div class="breadcrumb">
    <span class="breadcrumb__parent"><a href="{{ env('APP_URL') }}">Home</a></span>
    <img class="breadcrumb__divider" src="/img/bc_div.svg" alt="Breadcrumb Divider">
    <span class="breadcrumb__parent">Account</span>
    <img class="breadcrumb__divider" src="/img/bc_div.svg" alt="Breadcrumb Divider">
    <span class="breadcrumb__active">Order History</span>
</div>
@endsection --}}

@section("content")
<div class="account">
    <div class="account__sidebar">
        @include("account.partials._links")
    </div>
    <div class="account__body">
        <h2>Order</h2>

        <div class="confirmation__shipping">
            <div class="confirmation__shippingaddress">
                <span class="confirmation__totalstitle">Shipping Address</span>
                <span>{{$order->shippingAddress->first_name." ".$order->shippingAddress->surname}}</span><br>
                <span>{{$order->shippingAddress->company}}</span><br>
                <span>{{$order->shippingAddress->address1}}</span><br>
                <span>{{$order->shippingAddress->address2}}</span><br>
                <span>{{$order->shippingAddress->suburb}}</span><br>
                <span>{{$order->shippingAddress->state}}</span><br>
                <span>{{$order->shippingAddress->postcode}}</span>
            </div>
            <div class="confirmation__billingaddress">
                <span class="confirmation__totalstitle">Billing Address</span>
                <span>{{$order->billingAddress->first_name." ".$order->shippingAddress->surname}}</span><br>
                <span>{{$order->billingAddress->company}}</span><br>
                <span>{{$order->billingAddress->address1}}</span><br>
                <span>{{$order->billingAddress->address2}}</span><br>
                <span>{{$order->billingAddress->suburb}}</span><br>
                <span>{{$order->billingAddress->state}}</span><br>
                <span>{{$order->billingAddress->postcode}}</span>
            </div>
        </div>

        <div class="confirmation__summary">
            <div class="confirmation__items">
                <span class="confirmation__totalstitle">Order Summary</span>
                @foreach($order->items as $item)
                    <div class="confirmation__item">
                        <div class="confirmation__itemimg"
                            style="background-image: url('{{$item->variant->product->getFirstMediaUrl("images")}}')">
                        </div>
                        <div class="confirmation__itemname">
                            <a href="/products/{{$item->variant->product->slug}}">
                                {{$item->variant->product->name}}
                            </a>
                        </div>
                        <div class="confirmation__itemprice">
                            ${{$item->price}}
                        </div>
                        <div class="confirmation__itemqty">
                            <span id="product-quantity-{{$item->variant->product->id}}">{{$item->quantity}}</span>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="confirmation__totals">
                <span class="confirmation__totalstitle">Order Totals</span>
                <div class="confirmation__totalsitem">
                    <span>Items:</span> {{$order->cart_count}}
                </div>
                <div class="confirmation__totalsitem">
                    <span>Subtotal:</span>${{$order->item_total}}
                </div>
                <div class="confirmation__totalsitem">
                    <span>GST:</span>${{$order->item_total}}
                </div>
                @if($order->coupon)
                    <div class="confirmation__totalsitem">
                        <span>Coupon:</span>- ${{$order->coupon_discount}}
                    </div>
                @endif
                <div class="confirmation__totalsitem">
                    <span>Shipping:</span> ${{$order->shipping_total}}
                </div>
                <div class="confirmation__totalsitem cart__total">
                    <span>Total:</span>${{$order->order_total}}
                </div>
                <div class="confirmation__paymethod">
                    @if($order->payment_method == "credit_card")
                        <span><small>Paid via Credit Card</small></span>
                    @elseif($order->payment_method == "paypal")
                        <span><small>Paid via PayPal</small></span>
                    @elseif($order->payment_method == "afterpay")
                        <span><small>Paid via Afterpay</small></span>
                    @elseif($order->payment_method == "zip")
                        <span><small>Paid via Zip</small></span>
                    @endif
                </div>
            </div>
            <div class="confirmation__disclaimer">
                <p><small><strong>Please note:</strong> Confirmation is subject to stock levels, payment, description and price verification.</small>
                <small>If you have any queries regarding your order, please call our customer service team on <a href="tel:+61393569400">(03) 9356 9400</a> between 9am-5pm weekdays.</small>
                </p>
            </div>
        </div>
    </div>
</div>
@stop
