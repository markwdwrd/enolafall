@extends('layouts.master')

@section('page-title'){{ 'Registration' }}@stop
@section('page-id'){{ 'register' }}@stop

@section('breadcrumb')
@stop

@section('content')
    {!! Form::open(['route' => 'register',  'method' => 'POST']) !!}
        @csrf

        {!! Form::label('first_name', 'First Name') !!}
        {!! Form::text('first_name', old('first_name'), ['class' => 'form-control', 'required']) !!}
        <span class="error">{!! $errors->first('first_name') !!}</span>

        {!! Form::label('surname', 'Surname') !!}
        {!! Form::text('surname', old('surname'), ['class' => 'form-control', 'required']) !!}
        <span class="error">{!! $errors->first('surname') !!}</span>

        {!! Form::label('email', 'Email') !!}
        {!! Form::email('email', old('email'), ['class' => 'form-control', 'required']) !!}
        <span class="error">{!! $errors->first('email') !!}</span>

        {!! Form::label('password', 'Password') !!}
        {!! Form::password('password', null, ['class' => 'form-control', 'required']) !!}
        <span class="error">{!! $errors->first('password') !!}</span>

        {!! Form::label('password_confirmation', 'Confirm Password') !!}
        {!! Form::password('password_confirmation', null, ['class' => 'form-control', 'required']) !!}
        <span class="error">{!! $errors->first('password_confirmation') !!}</span>

        {!! Form::button('Submit', ['class' => 'button full-button', 'type' => 'submit']) !!}

    {!! Form::close() !!}
@stop

@section('inline-scripts')
@stop
