<nav>
    <ul>
        @if(auth()->user())
            <li><a href="{!! url('/account') !!}">Account</a></li>
            <li><a href="{!! url('/logout') !!}">Logout</a></li>
        @else
            <li><a href="{!! url('/register') !!}">Register</a></li>
            <li><a href="{!! url('/login') !!}">Login</a></li>
        @endif
        <li>
            <a href="{!! url('/cart') !!}">Your Cart</a>
            @include('partials._cart')
        </li>
    </ul>
</nav>