<div class="cart {!! $cart && count($cart->items) ? 'has-items' : '' !!}" id="preview-cart">
  <div class="cart-empty">
    Your cart is empty.
  </div>
  <ul class="cart-items">
    @if($cart)
      @foreach($cart->items as $item)
        <li id="variant-{!! $item->variant->id!!}" class="cart-item">
          <div class="item-details">
            <div class="name">{!! $item->variant->product->name !!}</div>
            <div class="summary">{!! $item->variant->summary !!}</div>
          </div>
          <div class="item-qty">{!! $item->quantity !!}</div>
          <div class="item-price">{!! '$' . $item->total !!}</div>
          <div class="item-remove">
            {!! Form::button('Remove from cart', ['data-variant' => $item->variant->id]) !!}
          </div>
        </li>
      @endforeach
    @endif
  </ul>
  <div class="cart-summary">
    <div class="totals">
      <div class="items">
        {!! $cart ? $cart->cart_count : '0 Items' !!}
      </div>
      <div class="total">
        ${!! $cart ? $cart->order_total : '0.00' !!}
      </div>
    </div>
  </div>
</div>

<script type="html/template" id="cart-item-template">
  <li id="variant-{id}">
    <div class="item-details">
      <div class="name">{productName}</div>
      <div class="summary">{variantSummary}</div>
    </div>
    <div class="item-qty">{itemQuantity}</div>
    <div class="item-price">{itemTotal}</div>
    <div class="item-remove">
      <button data-variant="{id}">Remove from cart</button>
    </div>
  </li>
</script>