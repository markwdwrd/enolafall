@extends('layouts.master')

@section('page-title'){!! $cmsPage->title !!}@stop
@section('page-id'){!! $cmsPage->slug !!}@stop
@section('meta-title'){!! $cmsPage->meta_title !!}@stop
@section('meta-keywords'){!! $cmsPage->meta_keywords !!}@stop
@section('meta-description'){!! $cmsPage->meta_description !!}@stop
@section('main-class'){{ 'cms-page' }}@stop

@section('og-image')
    @if($cmsPage->hasMedia('images'))
        <meta property="og:image" content="{!! $cmsPage->getMedia('images')->first()->getUrl() !!}">
    @endif
@stop

@section('slider')
    @include('pages.partials._slider', ["slider" => $cmsPage->slider])
@stop

@section('introduction')
    <section id="page-intro" style="background-image: url({!! $cmsPage->hasMedia('images') ?  $cmsPage->getMedia('images')->first()->getUrl() : '' !!});">
        <div class="container">
            <h1>{!! $cmsPage->title !!}</h1>
            <h2>{!! $cmsPage->subtitle !!}</h2>
            {!! $cmsPage->content !!}
            <nav>
                @include('partials._nav')
            </nav>
        </div>
        <div class="next">
            <a href="#" title="Next"></a>
        </div>
    </section>
@stop

@section('content')
    <section id="stream">
        <div class="container">
            <div class="video">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/GQKqH6cyZ9M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <a href="https://www.youtube.com/user/EnolaFall" title="Enola Fall's YouTube channel" target="_blank"><i class="icon youtube"></i> More videos</a>
            </div>
            <div class="audio">
                <iframe src="https://open.spotify.com/embed/track/5JGzuQUgWlTvqCp3cuzPF3" width="300" height="80" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>

                <iframe src="https://open.spotify.com/embed/track/62pUBhqH7SEh0xMCCfPx9o" width="300" height="80" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>

                <iframe src="https://open.spotify.com/embed/track/4EB3gNZSRN8L1Q8aFvv99F" width="300" height="80" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                <a href="https://open.spotify.com/artist/5Jj7why6grc7rehsSSCtmb?si=f6lSJslYTdypzi80jfLfLw" title="Enola Fall's Spotify artist page" target="_blank"><i class="icon spotify"></i> More tracks</a>
            </div>
        </div>
        <div class="next">
            <a href="#" title="Next"></a>
        </div>
    </section>
    @foreach($cmsPage->nodes as $i => $node)
        <section id="{!! $node->slug !!}">
            <div class="container focus-within:content-block cols-{!! $node->columns !!}">
            @foreach($node->nodeContents as $content)
                <div id="{{ $content->contentable->slug }}" class="{{ $content->type }}">
                    @include('pages.partials._'.$content->type, [$content->type => $content->contentable])
                </div>
            @endforeach
            @if($node->slug === 'contact')
                <ul class="links">
                    <li><a href="https://facebook.com/enolafall" target="_blank"><i class="icon facebook"></i>Facebook</a></li>
                    <li><a href="https://instagram.com/enolafall/" target="_blank"><i class="icon instagram"></i>Instagram</a></li>
                    <li><a href="https://twitter.com/enolafallband" target="_blank"><i class="icon twitter"></i>Twitter</a></li>
                    <li><a href="http://youtube.com/enolafall"><i class="icon youtube"></i>YouTube</a></li>
                    <li><a href="https://open.spotify.com/artist/5Jj7why6grc7rehsSSCtmb?si=f6lSJslYTdypzi80jfLfLw" target="_blank"><i class="icon spotify"></i>Spotify</a></li>
                    <li><a href="https://enolafall.bandcamp.com" target="_blank"><i class="icon bandcamp"></i>Bandcamp</a></li>
                </ul>
            @endif
            </div>
            @if (count($cmsPage->nodes) > $i + 1)
                <div class="next">
                    <a href="#" title="Next"></a>
                </div>
            @endif
        </section>
    @endforeach
@stop

@section('inline-scripts')
    <script type="text/javascript">
        $(window).on('scroll', function () {
            const header = $('body > header');
            const scroll = $(window).scrollTop();
            const threshold = header.height();
            if (scroll > threshold) {
                header.addClass('show');
            } else {
                header.removeClass('show');
            }
        });
        $(document).on('click', 'section > .next > a', function () {
            const next = $(this).closest('section').next('section');
            $('html, body').animate({
                scrollTop: next.offset().top
            }, 1000);
        });
    </script>
@stop
