@extends('layouts.master')

@section('page-title'){!! $cmsPage->title !!}@stop
@section('page-id'){!! $cmsPage->slug !!}@stop
@section('meta-title'){!! $cmsPage->meta_title !!}@stop
@section('meta-keywords'){!! $cmsPage->meta_keywords !!}@stop
@section('meta-description'){!! $cmsPage->meta_description !!}@stop
@section('main-class'){{ 'cms-page' }}@stop

@section('slider')
    @include('pages.partials._slider', ["slider" => $cmsPage->slider])
@stop

@section('introduction')
    <section class="page-header"@if ($cmsPage->hasMedia('images')) @foreach($cmsPage->getMedia('images') as $image) style="background-image: url('/uploads/page/{{ $image->id }}/{{ $image->file_name }}');"@endforeach @endif>
        <h1>{!! $cmsPage->title !!}</h1>
    </section>

    <section id="page-intro">
        {!! $cmsPage->content !!}
    </section>
@stop

@section('breadcrumb')
    <li>{!! $cmsPage->title!!}</li>
@stop

@section('content')
    @foreach($cmsPage->nodes as $node)
        <section id="{!! $node->slug !!}" class="content-block cols-{!! $node->columns !!}">
            @foreach($node->nodeContents as $content)
                <section id="{{ $content->contentable->slug }}" class="{{ $content->type }}">
                    @include('pages.partials._'.$content->type, [$content->type => $content->contentable])
                </section>
            @endforeach
        </section>
    @endforeach
@stop

@section('inline-scripts')
@stop
