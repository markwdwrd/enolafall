@extends('layouts.master')

@section('page-title'){!! $cmsPage->title !!}@stop
@section('page-id'){!! $cmsPage->slug !!}@stop
@section('meta-keywords'){!! $cmsPage->meta_keywords !!}@stop
@section('meta-description'){!! $cmsPage->meta_description !!}@stop
@section('main-class')@stop

@section('content')
    {!! $cmsPage->content !!}

    @if (count($people) > 0)
        @foreach($people as $person)
            {{-- $person --}}
        @endforeach

        {!! $posts->render() !!}
    @else
        <p>There are no people to display</p>
    @endif
@stop

@section('inline-scripts')
@stop
