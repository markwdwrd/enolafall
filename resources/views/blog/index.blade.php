@extends('layouts.master')

@section('page-title'){!! $cmsPage->title !!}@stop
@section('page-id'){!! $cmsPage->slug !!}@stop
@section('meta-keywords'){!! $cmsPage->meta_keywords !!}@stop
@section('meta-description'){!! $cmsPage->meta_description !!}@stop
@section('main-class')@stop

@section('content')
    {!! $cmsPage->content !!}

    @if (isset($tag))
        <p><strong>Posts tagged: {!! $tag->name !!}</strong></p>
    @endif

    @foreach($categories as $category)
        <a href="/blog?category={{$category->slug}}">{{$category->name}}</a>
    @endforeach

    @if (count($posts) > 0)
        @foreach($posts as $post)
            <a href="/blog/{{$post->slug}}">{{$post->title}}</a>
        @endforeach
        {!! $posts->render() !!}
    @else
        <p>There are no posts to display</p>
    @endif
@stop

@section('inline-scripts')
@stop
