@extends('layouts.master')

@section('page-title'){{ 'Checkout' }}@stop
@section('page-id'){{ 'checkout' }}@stop
@section('main-class'){{ 'cms-page' }}@stop

@section('introduction')
    <section class="page-header">
    </section>
@stop


@section('content')
<section id="cms-wrapper">
    <div class="row checkout-container">
        <div class="login">
            {!! Form::open(['url' => 'login', 'class' => 'card']) !!}
                <h2>Login</h2>
                <p><small>Checkout using your account</small></p>

                @csrf

                {!! Form::label('email', 'Email *') !!}
                {!! Form::text('email', null, ['id' => 'email', 'placeholder' => 'Email', 'data-validation' => 'req']) !!}
                <span class="field-error">{!! $errors->first('email') !!}</span>

                {!! Form::label('password', 'Password *') !!}
                {!! Form::password('password', ['id' => 'password', 'class' => 'field-input', 'placeholder' => 'Password', 'data-validation' => 'req']) !!}
                <span class="field-error">{!! $errors->first('password') !!}</span>

                <label for="remember">
                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    Remember Me
                </label>
                {!! Form::button('Login to Account', ['type' => 'submit', 'class' => 'w-full button green']) !!}
            {!! Form::close() !!}

            <a href="{{ route('password.request') }}">Forgot your password?</a>
            @include('admin.partials._messages')
        </div>
        <div class="guest">
            {!! Form::open(['route' => 'checkout.email', 'method' => 'POST']) !!}
                <h2>Guest Checkout</h2>
                <p><small>Checkout without creating an account</small></p>

                {!! Form::label('email', 'Email Address') !!}
                {!! Form::text('email', $order->email) !!}

                <span class="{!! $errors->has('email') ? 'error' : '' !!}">{!! $errors->first('email') !!}</span>
                {!! Form::button('Checkout as Guest', ['class' => 'button', 'type' => 'submit']) !!}
            {!! Form::close() !!}
        </div>
    </div>
</section>
@stop


@section('inline-scripts')
    <script>
        $('#email-form').validate({
            errorElement: 'span',
            rules: {
                email: {
                    required: true,
                    email: true
                }
            }
        });
    </script>
@stop
