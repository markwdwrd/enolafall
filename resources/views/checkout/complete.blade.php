@extends("layouts.utility")

@section('page-title')Order Confirmation @stop
@section('page-id') complete @stop

@section("content")
    <div class="confirmation">
        <div class="confirmation__success">
            <div class="confirmation__successtext">
                <h1>Thanks for your order!</h1>
                <h2>Your order <strong>#{!!  $order->id  !!}</strong> has been placed.</h2>
                <p>We have sent a confirmation email to <strong>{!!  $order->email  !!} </strong>.</p>

                @auth
                <p>You can view the details of this order any time <a href="/account/order-history/">in your
                        account</a>.</p>
                @endauth
                @guest
                <p><a href="/register">Sign up</a> for an account for faster checkout</p>
                @endguest
            </div>
        </div>

        <div class="confirmation__summary">
            <div class="confirmation__items">
                <span class="confirmation__totalstitle">Order Summary</span>
                @foreach($order->items as $item)
                <div class="confirmation__item">
                    <div class="confirmation__itemimg"
                        style="background-image: url('{!! $item->variant->product->getFirstMediaUrl("images") !!}')">
                    </div>
                    <div class="confirmation__itemname">
                        <a href="/products/{!! $item->variant->product->slug !!}">
                            <small>{!! $item->variant->product->name !!}</small>
                        </a>
                    </div>
                    <div class="confirmation__itemprice">
                        <small>${!! $item->price !!}</small>
                    </div>
                    <div class="confirmation__itemqty">
                        <small><span id="product-quantity-{!! $item->variant->product->id !!}">{!! $item->quantity !!}</span></small>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="confirmation__totals">
                <span class="confirmation__totalstitle">Order Totals</span>
                <div class="confirmation__totalsitem">
                    <span>Items:</span> {!! $order->cart_count !!}
                </div>
                <div class="confirmation__totalsitem">
                    <span>Subtotal:</span>${!! $order->item_total !!}
                </div>
                @if($order->coupon)
                    <div class="confirmation__totalsitem">
                        <span>Coupon:</span>-${!! $order->coupon_discount !!}
                    </div>
                    <p><a href="/checkout/remove_coupon" class="button remove-coupon">Remove</a></p>
                @endif
                <div class="confirmation__totalsitem">
                    <span>Shipping:</span> ${!! $order->shipping_total !!}
                </div>
                <div class="confirmation__totalsitem cart__total">
                    <span>Total:</span>${!! $order->order_total !!}
                </div>
                <div class="confirmation__paymethod">
                    @if($order->payment_method == "credit_card")
                        <span><small>Paid via Credit Card</small></span>
                    @elseif($order->payment_method == "paypal")
                        <span><small>Paid via PayPal</small></span>
                    @endif
                </div>
            </div>
        </div>

        <div class="confirmation__shipping">
            <div class="confirmation__shippingaddress">
                <span class="confirmation__totalstitle">Shipping Address</span>
                <span>{!! $order->shippingAddress->first_name . ' ' . $order->shippingAddress->surname !!}</span><br>
                <span>{!! $order->shippingAddress->company !!}</span><br>
                <span>{!! $order->shippingAddress->address1 !!}</span><br>
                <span>{!! $order->shippingAddress->address2 !!}</span><br>
                <span>{!! $order->shippingAddress->suburb !!}</span><br>
                <span>{!! $order->shippingAddress->state !!}</span><br>
                <span>{!! $order->shippingAddress->postcode !!}</span>
            </div>
            <div class="confirmation__billingaddress">
                <span class="confirmation__totalstitle">Billing Address</span>
                <span>{!! $order->billingAddress->first_name . ' ' . $order->shippingAddress->surname !!}</span><br>
                <span>{!! $order->billingAddress->company !!}</span><br>
                <span>{!! $order->billingAddress->address1 !!}</span><br>
                <span>{!! $order->billingAddress->address2 !!}</span><br>
                <span>{!! $order->billingAddress->suburb !!}</span><br>
                <span>{!! $order->billingAddress->state !!}</span><br>
                <span>{!! $order->billingAddress->postcode !!}</span>
            </div>
        </div>

    </div>
@stop


@section("inline-scripts")
@stop
