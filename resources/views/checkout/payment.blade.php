@extends('layouts.master')

@section('page-title'){{ 'Checkout' }}@stop
@section('page-id'){{ 'checkout' }}@stop
@section('main-class'){{ 'cms-page' }}@stop

@section('introduction')
    <section class="page-header">
    </section>
@stop

@section('content')
<section id="cms-wrapper">
    <div class="row checkout-container">
        {!! Form::open(['route' => 'checkout.payment', 'method' => 'POST', 'id' => 'checkout-payment']) !!}

            <label>{!! Form::radio('payment_method', 'credit_card', ['checked']) !!} Credit Card</label>
            <label>{!! Form::radio('payment_method', 'paypal') !!} PayPal</label>
            @if($errors->has('payment_method'))
                <span class='error'>{!! $errors->first('payment_method') !!}</span>
            @endif
            <div class="payment-method" id="credit_card">
                {!! Form::label('card_name', 'Name on Card') !!}
                {!! Form::text('card_name', null, ['id' => 'card_name']) !!}
                <span class="error">{!! $errors->first('card_name') !!}</span>

                {!! Form::label('card_number', 'Credit Card Number') !!}
                {!! Form::text('card_number', null, ['id' => 'card_number']) !!}
                <span class="error">{!! $errors->first('card_number') !!}</span>

                {!! Form::label('card_expiry', 'Expiry') !!}
                {!! Form::text('card_expiry_month', null, ['id' => 'card_expiry_month']) !!}
                {!! Form::text('card_expiry_year', null, ['id' => 'card_expiry_year']) !!}
                <span class="error">{!! $errors->first('card_expiry_month') !!} {!! $errors->first('card_expiry_year') !!}</span>

                {!! Form::label('card_cvv', 'CVV') !!}
                {!! Form::text('card_cvv', null, ['id' => 'card_cvv']) !!}
                <span class="error">{!! $errors->first('card_cvv') !!}</span>
            </div>

            <div class="payment-method" id="paypal" hidden>
                <p>You will be redirected to PayPal to complete your purchase.</p>
                <p>Do not close your browser until after you have been redirected back to this site.
            </div>
            {!! Form::button('Process Payment', ['class' => 'button full-button', 'type' => 'submit']) !!}
        {!! Form::close() !!}
    </div>
</section>

<ul>
    <li>Item total: {!! $order->item_total !!}</li>
    <li>Order total: {!! $order->subtotal !!}</li>
    <li>Shipping: {!! $order->shipping_total !!}</li>
    <li>Total: {!! $order->total !!}</li>
</ul>

@stop


@section("inline-scripts")
    <script type="text/javascript">
        $(document).on('change', 'input[name=payment_method]', (elem) => {
            $('.payment-method').hide();
            $('.payment-method#' + elem.target.value).show();
        });
    </script>
@stop
