<li id="variant-{!! $item->variant->id!!}" class="cart-item">
    <div class="item-image">
        {!! $item->variant->product->getMedia('images')->first() !!}
    </div>
    <div class="item-details">
        <div class="name">{!! $item->variant->product->name !!}</div>
        <div class="summary">{!! $item->variant->summary !!}</div>
    </div>
    <div class="item-qty">
        {!! Form::number('qty[' . $item->variant->id . ']', $item->quantity ) !!}
    </div>
    <div class="item-price">{!! '$' . $item->total !!}</div>
    <div class="item-remove">
        {!! Form::button('Remove from cart', ['data-variant' => $item->variant->id]) !!}
    </div>
</li>
