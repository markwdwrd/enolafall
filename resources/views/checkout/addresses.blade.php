@extends('layouts.master')

@section('content')
<div class="checkout">
    {!! Form::model($order, ['route' => 'checkout.addresses', 'method' => 'POST']) !!}
        <h3>Shipping Address</h3>
        <div id="shipping-address">
            @include('account.partials._address', ['prefix' => 'shipping'])
        </div>

        <h3>Billing Address</h3>
        <fieldset>
            {!! Form::checkbox('shipping_billing_same', null, true, ['id' => 'shipping_billing_same']) !!}
            {!! Form::label('shipping_billing_same', 'Same as shipping') !!}
        </fieldset>
        <div id="billing-address">
            @include('account.partials._address', ['prefix' => 'billing'])
        </div>

        @if(auth()->user())
            <fieldset>
                {!! Form::checkbox('update_user_addresses', null, false, ['id' => 'update_user_addresses']) !!}
                {!! Form::label('update_user_addresses', 'Update saved addresses') !!}
            </fieldset>
        @endif

        {!! Form::button('Next', ['type' => 'submit']) !!}
    {!! Form::close() !!}
    @include('checkout.partials._totals')
</div>
@stop

@section('inline-scripts')
@stop
