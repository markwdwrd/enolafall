@extends('layouts.master')

@section('content')
<div class="checkout">
    {!! Form::model($order, ['route' => 'checkout.shipping', 'method' => 'POST']) !!}
        {{-- <div>
            <h2>Shipping</h2>
            @foreach($order->items as $item)
                {!! $item->variant->shippingChargeType->shippingCharges !!}
            @endforeach

        </div> --}}
        <fieldset>
            @foreach($shippingMethods as $method)
                <label>
                    {!! Form::radio('shipping_method_id', $method->id) !!}
                    {{ $method->name }}
                </label>
            @endforeach
        </fieldset>
        {!! Form::button('Next', ['type' => 'submit']) !!}
    {!! Form::close() !!}
    @include('checkout.partials._totals')
</div>
@stop

@section('inline-scripts')
@stop
