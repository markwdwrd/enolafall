@extends('layouts.master')

@section('page-title'){{ 'Your Cart' }}@stop
@section('page-id'){{ 'your-cart' }}@stop
@section('main-class'){{ 'cms-page' }}@stop


@section('introduction')
    <section>
        <h2>Your Cart</h2>
    </section>
@stop

@section('content')
<section>
    <div class="cart {!! $order && count($order->items) ? 'has-items' : '' !!}" id="checkout-cart">
        @if(count($order->items))
            {!! Form::open(['route' => 'cart.update', 'method' => 'POST']) !!}
                <ul class="cart-items">
                    @foreach($order->items as $item)
                        @include('checkout.partials._cart_item')
                    @endforeach
                </ul>
                <div class="cart-summary">
                    <div class="totals">
                        <div class="total item">
                            Items: <span>{!! $order->cart_count !!}</span>
                        </div>
                        <div class="total cart">
                            Subtotal: <span>{!! '$' . $order->item_total !!}</span>
                        </div>
                        @if(!empty($order->shipping))
                            <div class="total shipping">
                                Shipping: <span>{!! '$' . $order->shipping_total !!}</span>
                            </div>
                        @endif
                        <div class="total order">
                            Total: <span id="cart-total">{!! '$' . $order->order_total !!}</span>
                        </div>
                    </div>
                </div>
                <a href="{!! url('checkout') !!}" class="button" title="Proceed to Checkout">Proceed to Checkout</a>
                {!! Form::button('Update cart', ['class' => 'button', 'type' => 'submit']) !!}
            {!! Form::close() !!}
        @else
            <div class="cart-empty">
                <h3>Your Cart is Empty</h3>
                <p>You don't appear to have added anything to your cart yet.</p>
            </div>
        @endif
    </div>
</section>
@stop


@section('inline-scripts')
    <script type="text/javascript">
    </script>
@stop
