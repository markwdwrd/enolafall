@extends('layouts.master')

@section('page-title'){!! $product->name !!}@stop
@section('page-id'){!! $product->slug !!}@stop
@section('meta-title'){!! $product->meta_title !!}@stop
@section('meta-keywords'){!! $product->meta_keywords !!}@stop
@section('meta-description'){!! $product->meta_description !!}@stop
@section('main-class'){!! 'cms-page' !!}@stop

@section('introduction')
    @if ($product->hasMedia('main_images'))
        <section class="page-header" style="background-image: url({!! $product->getMedia('main_images')->first()->getUrl() !!});background-size: cover;background-position: top;background-repeat: no-repeat;">
        </section>
    @else
        <section class="page-header"></section>
    @endif
@stop

@section('content')

<section>
    <h1>{!! $product->name !!}</h1>
    <div>
        {!! $product->isOnSale ? 'On Sale' : '' !!}
        {!! $product->content !!}
    </div>
        @include('products.partials._add-to-cart', ['product' => $product])
</section>


@stop

@section('inline-scripts')
@stop