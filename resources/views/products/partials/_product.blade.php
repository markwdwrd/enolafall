  <li>
      <a href="{!! url('shop/' . $product->slug) !!}" title="{!! $product->name !!}">
        {!! $product->getMedia('images')->first() !!}
      </a>
      <h4><a href="{!! url('shop/' . $product->slug) !!}" title="{!! $product->name !!}">{!! $product->name !!}</a></h4>
  </li>