<div id="add-to-cart">
  <div id="product-variant-form">
    {!! Form::select('variant', ['' => 'Please select'] + $product->variants->pluck('summary', 'id')->all(), null, ['id' => 'select-variant']) !!}
    {!! Form::number('qty', 1,  ['id' => 'variant-qty']) !!}
    {!! Form::button('Add to Cart') !!}
  </div>
  <table id="product-variant-options">
    <tbody>
      @foreach($product->variants as $variant)
        <tr id="variant_{!! $variant->id !!}" class="{!! $variant->isOnSale ? 'on-sale' : '' !!}">
          <td class="name">
            {!! $variant->summary !!}
            <p>{!! $variant->description !!}</p>
          </td>
          <td class="price">
            @if ($variant->isOnSale)
              <span class="price original">{!! $variant->getPrice() !!}</span>
              <span class="price sale">{!! $variant->getSalePrice() !!}</span>
            @else
              <span class="price">{!! $variant->getPrice() !!}</span>
            @endif
          </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>