@extends('layouts.master')

@section('page-title'){!! $cmsPage->title !!}@stop
@section('page-id'){!! $cmsPage->slug !!}@stop
@section('meta-title'){!! $cmsPage->meta_title !!}@stop
@section('meta-keywords'){!! $cmsPage->meta_keywords !!}@stop
@section('meta-description'){!! $cmsPage->meta_description !!}@stop
@section('main-class'){{ 'cms-page' }}@stop

@section('introduction')
    @if ($cmsPage->hasMedia('images'))
        <section class="page-header" style="background-image: url({!! $cmsPage->getMedia('images')->first()->getUrl() !!});background-size: cover;background-position: top;background-repeat: no-repeat;">
        </section>
    @else
        <section class="page-header"></section>
    @endif
@stop

@section('content')
<section>
  <h1>{!! $cmsPage->title !!}</h1>
  <h2>{!! $cmsPage->subtitle !!}</h2>

  <ul>
    @foreach($products as $product)
      @include('products.partials._product', ['product' => $product])
    @endforeach
  </ul>
  {!! $products->render('vendor.pagination.default') !!}
</section>
@stop

@section('inline-scripts')
<script type="text/javascript">
</script>
@stop