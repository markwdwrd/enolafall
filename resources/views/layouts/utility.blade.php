<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" @yield('item-scope')>
	<head>
		<title>@if (View::hasSection('page-title'))@yield('page-title') | @endif{{ config('app.name') }}</title>
		<meta charset="utf8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
		<meta name="title" content="@yield('meta-title')">
		<meta name="keywords" content="@yield('meta-keywords')">
		<meta name="description" content="@yield('meta-description')">
		<meta name="csrf-token" content="{{ csrf_token() }}">

        @yield('meta-other')
        @include('partials._meta')

        <link href="https://fonts.googleapis.com/css2?family=Manrope:wght@400;600&family=Zilla+Slab:wght@300;400;600&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="{{ mix('/css/vendor.css') }}">
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
        <script src="https://js.stripe.com/v3/"></script>
	</head>

    <body id="@yield('page-id')">
        <div class="preloader"></div>
        @include('partials._messages')
        @include('partials._header')

        @yield('slider')
        @yield('introduction')
        @yield('breadcrumb')
        <main class="@yield('main-class') checkout">
			@yield('content')
		</main>

		@include('partials._footer')

		<div class="overlay"></div>

		<script src="{{ mix('/js/manifest.js') }}"></script>
		<script src="{{ mix('/js/vendor.js') }}"></script>
		<script src="{{ mix('/js/app.js') }}"></script>

		{!! HTML::script('/js/jqvalidate-additional-methods.js') !!}

		<!-- Polyfill for ES6 Promises for IE11, UC Browser and Android browser support -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
			@yield('inline-scripts')
	</body>
</html>
