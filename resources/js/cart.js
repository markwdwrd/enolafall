/**
 * ------------------------------------------------------------------------------
 * Initialisation for cart/checkout components
 * ------------------------------------------------------------------------------
 */
$(function () {
  // Disable/hide empty state selects
  $('select.state-select').each((e, elem) => {
    if (!$(elem).children('option').length) {
      $(elem).closest('fieldset').attr('disabled', true).hide();
    }
  });
});

/**
 * ------------------------------------------------------------------------------
 * Baseline js events for cart/checkout components
 * ------------------------------------------------------------------------------
 */

// Add item to cart, dynamically update cart items/qtys/totals
$(document).on('click', '#add-to-cart button', () => {
  const variant = $('#add-to-cart select[name=variant]').val();
  const qty = $('#add-to-cart input[name=qty]').val();

  addToCart(variant, qty).then(data => {
    updateCartValues(data);
  }).catch(error => {
    console.error(error);
  }).finally(() => {});
});

// Remove item from cart, dynamically update cart items/qtys/totals
// Triggered from the user's preview cart
$(document).on('click', '#preview-cart .item-remove button', event => {
  const variant = $(event.target).data('variant');
  removeFromCart(variant).then(data => {
    updateCartValues(data);
    $(event.target).closest('li').remove();
  }).catch(error => {
    console.error(error);
  }).finally(() => {});
});

// Remove item from cart, refresh to update cart items/qtys/totals
// Triggered from the user's checkout cart
$(document).on('click', '#checkout-cart .item-remove button', event => {
  const variant = $(event.target).data('variant');
  removeFromCart(variant).then(data => {
    location.reload();
  }).catch(error => {
    console.error(error);
  }).finally(() => {});
});

// Populate state select options when country is selected
$(document).on('change', 'select.country-select', event => {
  getStates(event.target.value).then(states => updateStateSelect(event.target, states));
});

// Show/hide the billing address fields based on checkbox
$(document).on('change', '#shipping_billing_same', function () {
  if ($(this).is(':checked')) {
    $('#billing-address').hide();
  } else {
    $('#billing-address').show();
  }
});

/**
 * ------------------------------------------------------------------------------
 * DOM manipulation for dynamic art updates
 * ------------------------------------------------------------------------------
 */

// Dynamic update of user's preview cart
const updateCartValues = (data) => {
  $('#preview-cart .cart-summary .items').html(data.cart_count);
  $('#preview-cart .cart-summary .total').html(data.total);
  if (data.is_empty) {
    $('#preview-cart').removeClass('has-items');
  } else {
    $('#preview-cart').addClass('has-items');
  }
  if (data.item) {
    insertCartItem(data.item);
  }

  $('#preview-cart').addClass('active');
  setTimeout(() => $('#preview-cart').removeClass('active'), 2000);
}

// Dynamic insert of item into user's preview cart
const insertCartItem = (item) => {
  var current = $(`#preview-cart .cart-items #variant-${item.id}`)
  if (current.length) {
    current.find('.qty').html(item.quantity);
    current.find('.price').html(item.total);
  } else {
    var template = $('#cart-item-template').html()
      .replace(/{id}/g, item.id)
      .replace(/{productName}/g, item.name)
      .replace(/{variantSummary}/g, item.summary)
      .replace(/{itemQuantity}/g, item.quantity)
      .replace(/{itemTotal}/g, item.total);
    $(template).appendTo($('#preview-cart .cart-items'));
  }
}

// Create and insert options from states array
const updateStateSelect = (elem, states) => {
  const stateSelect = $('#' + $(elem).data('target'));
  let options = '';
  stateSelect.find('option').remove();
  if (Object.keys(states).length) {
    options += '<option value="">Please select...</option>';
    $.each(states, function (key, value) {
      options += '<option value="' + key + '">' + value + '</option>';
    });
    stateSelect.append(options).closest('fieldset').removeAttr('disabled').show();
  } else {
    stateSelect.closest('fieldset').attr('disabled', true).hide();
  }
}


/**
 * ------------------------------------------------------------------------------
 * Actions for setting/getting data
 * ------------------------------------------------------------------------------
 */

const getVariantById = id => {
  return new Promise ((resolve, reject) => {
    $.get(`/variant/${id}`)
      .done(response => resolve(JSON.parse(response)))
      .fail(error => reject(error.responseJSON));
  });
}

const addToCart = (id, qty) => {
  return new Promise((resolve, reject) => {
    $.post(`/cart/add`, { variant: id, quantity: qty})
      .done(response => resolve(response))
      .fail(error => reject(error.responseJSON));
  });
}

const removeFromCart = (id) => {
  return new Promise((resolve, reject) => {
    $.post(`/cart/remove`, { variant: id })
      .done(response => resolve(response))
      .fail(error => reject(error.responseJSON));
  });
}

const getStates = (code) => {
  return new Promise((resolve, reject) => {
    $.get(`/countries/${code}/states`)
    .done(response => resolve(response))
    .fail(error => reject(error.responseJSON));
  })
}
