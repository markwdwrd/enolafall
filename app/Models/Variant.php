<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\InteractsWithMedia;

class Variant extends Model implements HasMedia
{
    use Sluggable, SoftDeletes, InteractsWithMedia;

    protected $table = 'variants';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'product_id', 'shipping_charge_type_id', 'name', 'sku', 'price', 'sale_price', 'description', 'position', 'is_active', 'sort_order'
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id')->withTrashed();
    }

    public function taxons()
    {
        return $this->belongsToMany('App\Models\Taxon', 'variant_taxon', 'variant_id', 'taxon_id');
    }

    public function shippingChargeType()
    {
        return $this->belongsTo('App\Models\ShippingChargeType', 'shipping_charge_type_id');
    }

    public function getSummaryAttribute()
    {
        if ($this->name) {
            return $this->name;
        }
        return implode(' / ', $this->taxons->pluck('name')->toArray());
    }

    public function getVariantPriceAttribute()
    {
        return $this->sale_price ? $this->sale_price : $this->price;
    }

    public function getPrice()
    {
        return $this->price ? '$' . number_format($this->price, 2) : null;
    }

    public function getSalePrice()
    {
        return $this->sale_price ? '$' . number_format($this->sale_price, 2) : null;
    }

    public function getIsOnSaleAttribute()
    {
        return !!$this->sale_price;
    }
}
