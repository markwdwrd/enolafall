<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingChargeType extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function shippingCharges()
    {
        return $this->hasMany('App\Models\ShippingCharge', 'shipping_charge_type_id');
    }
}
