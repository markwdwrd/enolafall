<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';

    protected $fillable = ['iso_name', 'iso', 'iso3', 'name', 'numcode', 'states_required'];
}
