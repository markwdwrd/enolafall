<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'order_items';

    protected $fillable = [
        'variant_id',
        'order_id',
        'quantity',
        'details',
        'price',
        'total'
    ];

    protected $casts = [
        'details' => 'json'
    ];

    public function variant()
    {
        return $this->belongsTo("App\Models\Variant", "variant_id");
    }

    public function order()
    {
        return $this->belongsTo("App\Models\Order", "order_id");
    }

    public function getShippingChargesAttribute()
    {
        return $this->variant->shippingCharges;
    }

    public function getTotalAttribute()
    {
        $total = $this->variant->variant_price * $this->quantity;

        return number_format($total, 2);
    }
}
