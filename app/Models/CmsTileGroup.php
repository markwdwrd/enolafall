<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CmsTileGroup extends Model
{
    protected $table = 'cms_tile_groups';

    protected $fillable = ['layout'];

    public $timestamps = false;

    public function tile_1()
    {
        return $this->belongsToMany("App\Models\Tile", "cms_tile_groups_tiles", "tile_group_id", "tile_id")
            ->wherePivot("tile_no", 1)
            ->orderBy("order", "ASC");
    }

    public function tile_2()
    {
        return $this->belongsToMany("App\Models\Tile", "cms_tile_groups_tiles", "tile_group_id", "tile_id")
            ->wherePivot("tile_no", 2)
            ->orderBy("order", "ASC");
    }

    public function tile_3()
    {
        return $this->belongsToMany("App\Models\Tile", "cms_tile_groups_tiles", "tile_group_id", "tile_id")
            ->wherePivot("tile_no", 3)
            ->orderBy("order", "ASC");
    }

    public function tile_4()
    {
        return $this->belongsToMany("App\Models\Tile", "cms_tile_groups_tiles", "tile_group_id", "tile_id")
            ->wherePivot("tile_no", 4)
            ->orderBy("order", "ASC");
    }
}
