<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const CART = 'cart';
    const SUBMITTED = 'submitted';
    const CONFIRMED = 'confirmed';
    const PAYMENT = 'payment';
    const COMPLETE = 'complete';
    const HOLD = 'hold';
    const CANCELLED = 'cancelled';
    const SHIPPED = 'shipped';
    const REFUNDED = 'refunded';

    protected $table = 'orders';

    protected $fillable = [
        'user_id',
        'email',
        'state',
        'completed_at',
        'token',
        'billing_address_id',
        'shipping_address_id',
        'item_total',
        'shipping_total',
        'subtotal',
        'tax',
        'total',
        'coupon_id',
        'payment_method',
        'payment_reference'
    ];

    protected $dates = ['completed_at'];

    public function items()
    {
        return $this->hasMany('App\Models\OrderItem', 'order_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function billingAddress()
    {
        return $this->belongsTo('App\Models\Address', 'billing_address_id');
    }

    public function shippingAddress()
    {
        return $this->belongsTo('App\Models\Address', 'shipping_address_id');
    }

    public function coupon()
    {
        return $this->belongsTo('App\Models\Coupon', 'coupon_id');
    }

    public function getItemCountAttribute()
    {
        return array_sum($this->items->pluck('quantity')->all());
    }

    public function getOrderTotalAttribute()
    {
        $total = str_replace(',', '', $this->item_total);

        if ($this->coupon && $this->coupon->sign === '$')
            $total = $total - $this->coupon->discount;
        else if ($this->coupon && $this->coupon->sign === '%')
            $total = $total - ($total * ($this->coupon->discount / 100));

        if ($total < 0)
            $total = 0;

        return number_format($total, 2);
    }

    public function getCartCountAttribute()
    {
        return $this->item_count . ' ' . ($this->item_count !== 1 ? 'Items' : 'Item');
    }

    public function getIsPaidAttribute()
    {
        return in_array($this->state, ['complete', 'shipped', 'paid']);
    }

    public function getCouponDiscountAttribute()
    {
        if (!$this->coupon)
            return 0;

        if ($this->coupon->sign === '$')
            return $this->coupon->discount;

        $total = 0;
        foreach ($this->items as $item)
            $total += ($item->variant->variant_price * $item->quantity);

        return number_format($total * ($this->coupon->discount / 100), 2);
    }
}
