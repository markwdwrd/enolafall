<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Taxon extends Model
{
    use Sluggable;

    protected $table = 'taxons';

    protected $fillable = ['taxonomy_id', 'parent_id', 'name', 'slug', 'permalink'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ],
            'permalink' => [
                'source' => ['taxonomy.slug', '/', 'slug'],
                'separator' => '-'
            ],
        ];
    }

    public function taxonomy()
    {
        return $this->belongsTo("App\Models\Taxonomy", "taxonomy_id");
    }

    public function children()
    {
        return $this->hasMany("App\Models\Taxon", "parent_id", "id")->orderBy("name", "ASC");
    }

    public function parent()
    {
        return $this->belongsTo("App\Models\Taxon", "parent_id", "id");
    }

    public function products()
    {
        return $this->belongsToMany("App\Models\Product", 'product_taxon', 'taxon_id', 'product_id');
    }
}
