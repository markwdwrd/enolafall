<?php

namespace App\Models;

use App\Scopes\ActiveProductsScope;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Product extends Model implements HasMedia
{
    use Sluggable, SoftDeletes, InteractsWithMedia;

    const PRODUCT_TYPES = ['variant', 'simple'];

    const SPECIFICATION_TYPES = [];

    protected $table = 'products';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'slug', 'type', 'sku', 'price', 'sale_price', 'introduction', 'description',
        'meta_title', 'meta_keywords', 'meta_description', 'is_active'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveProductsScope);
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function variants()
    {
        return $this->hasMany('App\Models\Variant', 'product_id')->orderBy('sort_order');
    }

    public function taxons()
    {
        return $this->belongsToMany('App\Models\Taxon', 'product_taxon', 'product_id', 'taxon_id');
    }

    public function productSpecifications()
    {
        return $this->hasMany('App\Models\ProductSpecification', 'product_id')->with('specification');
    }

    public function related_products()
    {
        return $this->belongsToMany('App\Models\Product', 'product_related_products', 'product_id', 'related_product_id');
    }

    public function getPrice()
    {
        return $this->price ? '$' . number_format($this->price, 0) : null;
    }

    public function getSalePrice()
    {
        return $this->sale_price ? '$' . number_format($this->sale_price, 0) : null;
    }

    public function getIsOnSaleAttribute()
    {
        if ($this->type === 'variant') {
            return !!$this->variants->whereNotNull('sale_price');
        }
        return !!$this->sale_price;
    }

    public function getVariantsStringsAttribute()
    {
        if ($this->variants) {
            return implode(', ', $this->variants->pluck('sku')->toArray());
        }
    }

    public function getTaxonsStringAttribute()
    {
        if ($this->taxons) {
            return implode(', ', $this->taxons->pluck('name')->toArray());
        }
    }

    public function getSpecificationsAttribute()
    {
        $specifications = [];
        foreach ($this::SPECIFICATION_TYPES as $t => $type) {
            $specifications[$t] = collect($this->productSpecifications->filter(function ($value) use ($t) {
                return $value->type === $t;
            })->all());
        }
        return collect($specifications);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(config('sitesettings.thumb_width'));
    }
}
