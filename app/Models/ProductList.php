<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class ProductList extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $table = 'product_lists';

    protected $fillable = ['name', 'description', 'featured', 'button_text', 'button_link', 'button_colour', 'order'];

    public function products()
    {
        return $this->belongsToMany("App\Models\Product", "product_list_products", "product_list_id", "product_id");
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('background')->useDisk('product_lists')->singleFile();
    }
}
