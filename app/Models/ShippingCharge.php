<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingCharge extends Model
{
    use HasFactory;

    protected $fillable = [
        'shipping_charge_type_id',
        'shipping_method_id',
        'country_iso',
        'base_charge',
        'additional_charge'
    ];
}
