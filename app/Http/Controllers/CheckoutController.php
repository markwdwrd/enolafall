<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\User;
use App\Models\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Omnipay\Common\Exception\InvalidCreditCardException;
use Omnipay\Omnipay;
use App\Mail\OrderComplete;
use App\Mail\OrderPlaced;
use Illuminate\Support\Str;
use App\Models\Address;
use App\Models\Coupon;
use App\Models\ShippingMethod;
use DougSisk\CountryState\CountryState;
use Validator;

class CheckoutController extends Controller
{
  /**
   * Create a new instance of CheckoutController
   *
   */
  public function __construct()
  {
    $this->countries = new CountryState();
  }

  /**
   * Get our current order
   *
   * @return \App\Order | null
   */
  public function getOrder()
  {
    $order = Order::where('token', session('token'))->first();

    foreach ($order->items as $item) {
      $item->update([
        'details'  => [
          'product'   => $item->variant->product->name,
          'variant'   => $item->variant->name ? $item->variant->name : '',
          'sku'       => $item->variant->sku
        ],
        'price'   =>  $item->variant->variantPrice,
        'total'   =>  $item->quantity * $item->variant->variantPrice
      ]);
    }
    $order->update([
      'item_total'  => $order->items->sum('total'),
      'subtotal'    => $order->items->sum('total'), // TODO: item_total - discount
      'tax'         => $order->items->sum('total') * (0.11), // TODO: use rate in sitesettings
      'total'       => $order->items->sum('total') + $order->shipping_total
    ]);

    return $order;
  }

  /**
   * Checkout screen
   *
   * @return \Illuminate\Http\RedirectResponse
   */
  public function checkout()
  {
    $order = $this->getOrder();

    if (!$order->user) {
      return view('checkout.email', compact('order'));
    }

    return redirect()->to('/checkout/addresses');
  }

  /**
   * Apply a coupon
   *
   * @param Request $request
   * @return \Illuminate\Http\RedirectResponse
   */
  public function applyCoupon(Request $request)
  {
    $coupon = Coupon::where('code', $request->coupon)->first();
    if (!$coupon)
      return redirect()->back()->with('error', "Sorry, that doesn't look like a valid coupon code");
    if ($coupon->date_start == null || $coupon->date_expires == null || Carbon::now() < $coupon->date_start || Carbon::now() > $coupon->date_expires)
      return redirect()->back()->with('error', "Sorry, that doesn't look like a valid coupon code");

    $order = $this->getOrder();
    if ($coupon->products->count() > 0 && count(array_intersect($coupon->products->pluck('id')->all(), $order->items->pluck('variant.product.id')->all())) < 1)
      return redirect()->back()->with('error', "Sorry, this coupon only applies to certain products");
    $order->update(['coupon_id' => $coupon->id]);

    return redirect()->back()->with('success', "You have successfully applied your coupon!");
  }

  /**
   * Remove the coupon from a cart
   *
   * @return \Illuminate\Http\Response
   */
  public function removeCoupon()
  {
    $order = $this->getOrder();
    $order->update(['coupon_id' => null]);

    return redirect()->back()->with('success', "You have successfully removed your coupon!");
  }

  /**
   * Update email via AJAX
   *
   * @param Request $request
   * @return \Illuminate\Http\RedirectResponse
   */
  public function postEmail(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'email'    => 'required|email'
    ]);

    if ($validator->fails())
      return redirect()->back()->withErrors($validator);

    $order = $this->getOrder();
    $order->update(['email' => $request->email]);

    return redirect()->to('/checkout/addresses');
  }

  /**
   * Form for setting addresses
   *
   * @return \Illuminate\Http\Response
   */
  public function getAddresses()
  {
    $order = $this->getOrder();

    $countries = $this->countries->getCountries();

    $country = [];
    $country['shippingAddress'] = config('sitesettings.default_country');
    $country['billingAddress'] = config('sitesettings.default_country');

    $states = [];
    $states['shippingAddress'] = $this->countries->getStates(config('sitesettings.default_country'));
    $states['billingAddress'] = $this->countries->getStates(config('sitesettings.default_country'));

    if ($user = User::whereId(auth()->id())->first()) {
      $country['shippingAddress'] = $user->shippingAddress ? $user->shippingAddress->country_iso : config('sitesettings.default_country');
      $country['billingAddress'] = $user->billingAddress ? $user->billingAddress->country_iso : config('sitesettings.default_country');

      $states['shippingAddress'] = $this->countries->getStates($user->shippingAddress ? $user->shippingAddress->country_iso : config('sitesettings.default_country'));
      $states['billingAddress'] = $this->countries->getStates($user->billingAddress ? $user->billingAddress->country_iso : config('sitesettings.default_country'));
    }

    return view('checkout.addresses', compact('order', 'countries', 'country', 'states'));
  }

  /**
   * Update addresses
   *
   * @param Request $request
   * @return \Illuminate\Http\RedirectResponse
   */
  public function postAddresses(Request $request)
  {
    $validator = Validator::make($request->all(), [
      // Shipping Address
      'shippingAddress.first_name'    => 'required',
      'shippingAddress.surname'       => 'required',
      'shippingAddress.email'         => 'required|email',
      'shippingAddress.phone'         => 'required',
      'shippingAddress.address1'      => 'required',
      'shippingAddress.city_suburb'   => 'required',
      'shippingAddress.postcode'      => 'required',
      'shippingAddress.state'         => 'filled',
      'shippingAddress.country_iso'   => 'required',

      // Billing Addtress - only validate if not same as shipping
      'billingAddress.first_name'    => 'required_without:shipping_billing_same',
      'billingAddress.surname'       => 'required_without:shipping_billing_same',
      'billingAddress.email'         => 'required_without:shipping_billing_same|nullable|email',
      'billingAddress.phone'         => 'required_without:shipping_billing_same',
      'billingAddress.address1'      => 'required_without:shipping_billing_same',
      'billingAddress.city_suburb'   => 'required_without:shipping_billing_same',
      'billingAddress.postcode'      => 'required_without:shipping_billing_same',
      'billingAddress.state'         => 'filled',
      'billingAddress.country_iso'   => 'required_without:shipping_billing_same',
    ]);

    if ($validator->fails()) {
      return redirect()->back()->withInput()->withErrors($validator);
    }

    $order = $this->getOrder();
    $user = User::whereId(auth()->id())->first();

    if ($validator->fails()) {
      return redirect()->back()->withInput()->withErrors($validator);
    }

    // Create new shipping address if not set for order or request doesn't match existing
    if (!$order->shippingAddress || $order->shippingAddress->only($order->shippingAddress->getFillable()) != $request->shippingAddress) {
      $shippingAddress = Address::create($request->shippingAddress);
      $order->shippingAddress()->associate($shippingAddress);
    }

    // Copy shipping address to billing address if they're to be the same
    if (isset($request->shipping_billing_same)) {
      $request->billingAddress = $request->shippingAddress;
    }

    // Create new billing address if not set for order or request doesn't match existing
    if (!$order->billingAddress || $order->billingAddress && $order->billingAddress->only($order->billingAddress->getFillable()) != $request->billingAddress) {
      $billingAddress = Address::create($request->billingAddress);
      $order->billingAddress()->associate($billingAddress);
    }

    // Update the user's saved addresses if requested
    if ($user && isset($request->update_user_addresses)) {
      if ($shippingAddress) {
        $user->shippingAddress()->associate($shippingAddress);
      }
      if ($billingAddress) {
        $user->billingAddress()->associate($billingAddress);
      }
      $user->save();
    }

    $order->save();

    return redirect()->to('/checkout/shipping');
  }

  /**
   * Let user choose their shipping
   *
   * @return \Illuminate\Http\Response
   */
  public function getShipping()
  {
    $order = $this->getOrder();

    $shippingMethods = ShippingMethod::all();

    return view('checkout.shipping', compact('order', 'shippingMethods'));
  }

  /**
   * Update user's shipping
   *
   * @param Request $request
   * @return \Illuminate\Http\Response
   */
  public function postShipping(Request $request)
  {
    $order = $this->getOrder();

    $validator = Validator::make($request->all(), [
      'shipping_method_id' => 'required'
    ], [
      'shipping_method_id.required' => 'Please select your shipping method'
    ]);

    if ($validator->fails()) {
      return redirect()->back()->withErrors($validator);
    }

    $shippingCharges = [];
    foreach ($order->items as $item) {
      $charges = $item->variant->shippingChargeType->shippingCharges->where('shipping_method_id', $request->shipping_method_id);

      if (!$charge = $charges->where('country_iso', $order->shippingAddress->country_iso)->first()) {
        $charge = $charges->whereNull('country_iso')->first();
      }

      $shippingCharges[$item->id] = collect([
        'base_charge' => $charge->base_charge,
        'item_charge' => $item->quantity * $charge->additional_charge
      ]);
    }
    $base_charge = collect($shippingCharges)->max('base_charge');
    $item_charge = collect($shippingCharges)->sum('item_charge');

    $order->update([
      'shipping_total' => $base_charge + $item_charge
    ]);

    return redirect()->to('/checkout/payment');
  }

  /**
   * Show checkout.
   *
   * @return \Illuminate\Http\Response
   */
  public function getPayment()
  {
    $order = $this->getOrder();

    return view('checkout.payment', compact('order'));
  }

  /**
   * Post order and payment to gateway
   *
   * @param Request $request
   *
   * @return \Illuminate\Http\RedirectResponse
   */
  public function postPayment(Request $request)
  {
    $order = Order::where('token', session('token'))->first();

    $validator = Validator::make($request->all(), [
      'payment_method'    => 'required',
      'card_name'         => 'requiredIf:payment_method,' . Payment::CREDIT_CARD,
      'card_number'       => 'requiredIf:payment_method,' . Payment::CREDIT_CARD . '|digits_between:15,16',
      'card_expiry_month' => 'requiredIf:payment_method,' . Payment::CREDIT_CARD . '|digits:2',
      'card_expiry_year'  => 'requiredIf:payment_method,' . Payment::CREDIT_CARD . '|digits:4',
      'card_cvv'          => 'requiredIf:payment_method,' . Payment::CREDIT_CARD . '|digits_between:3,4',
    ], [
      'card_name.required_if'         => 'The card name field is required.',
      'card_number.required_if'       => 'The card number field is required.',
      'card_expiry_month.required_if' => 'The card expiry month field is required.',
      'card_expiry_year.required_if'  => 'The card expiry year field is required.',
      'card_cvv.required_if'          => 'The card cvv field is required.'
    ]);

    if ($validator->fails()) {
      return redirect()->back()->withInput()->withErrors($validator);
    }

    $order->update([
      'payment_method'    => $request->payment_method,
      'state'             => Order::PAYMENT
    ]);

    $payment = Payment::create([
      'amount'            => $order->orderTotal,
      'order_id'          => $order->id,
      'payment_method'    => $order->payment_method,
      'state'             => Payment::NEW_PAYMENT,
    ]);

    // Set up gateway base on payment type --- abort if none set
    if (!$gateway = $this->setupPaymentGateway($request->payment_method)) {
      return redirect()->back()->withInput()->with('error', "Couldn't set payment gateway!");
    }

    $purchase = [
      'amount'      => $order->total,
      'currency'    => 'AUD',
      'name'        => config('app.name') . ': Order Number - ' . $order->id,
      'description' => config('app.name') . ': Order Number - ' . $order->id . ' Payment ID - ' . $payment->id . ', Charge for ' . $order->email,
    ];

    // Handle credit card checkout
    if ($request->payment_method === Payment::CREDIT_CARD) {
      // Set up card details.
      // May need to include address info for verifcation purposes (possibly gateway/region dependant)
      // https://omnipay.thephpleague.com/api/cards/
      $card = [
        'number'      => $request->get('card_number'),
        'expiryMonth' => $request->get('card_expiry_month'),
        'expiryYear'  => $request->get('card_expiry_year'),
        'cvv'         => $request->get('card_cvv'),
      ];

      $purchase['card'] = $card;

      // Update payment with card details from form request.
      // Will be overwritten by the response or available for debugging payment failure
      $payment->update([
        'card_type'         => null, // figure it out? or just wait for response?
        'card_name'         => $request->get('card_name'),
        'card_expiry'       => $request->get('card_expiry_month') . '/' . $request->get('card_expiry_year'),
        'card_last_digits'  => Str::substr($request->get('card_number'), -4)
      ]);

      try {
        $response = $gateway->purchase($purchase)->send();
      } catch (InvalidCreditCardException $e) {
        $payment->update([
          'state'            => Payment::FAILED,
          'response_details' => response()->json($e->getMessage()),
        ]);
        return back()->withInput()->with(['error' => $e->getMessage()]);
      } catch (\Exception $e) {

        $payment->update([
          'state'            => Payment::FAILED,
          'response_details' => response()->json($e->getMessage()),
        ]);
        return back()->withInput()->with(['error' => "There was an error with your payment."]);
      }

      if ($response->isSuccessful()) {
        $data = $response->getData();
        return $this->paymentCallback($order, $payment, $data, $response->getTransactionReference());
      } else {
        $payment->update([
          'state'            => Payment::FAILED,
          'response_details' => response()->json($response->getData()),
        ]);
        return back()->withInput()->with(['error' => $response->getMessage()]);
      }
    }

    // Handle PayPal checkout
    if ($request->payment_method === Payment::PAYPAL) {
      // Set up purchase details for PayPal
      $purchase['cancelUrl'] = config('services.paypal.cancel_url');
      $purchase['returnUrl'] = config('services.paypal.return_url');

      try {
        $response = $gateway->purchase($purchase)->send();
      } catch (\Exception $e) {
        $payment->update([
          'state'             => Payment::FAILED,
          'response_details'  => response()->json($e->getMessage()),
        ]);

        $order->update(['payment_reference' => false]);
        return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
      }

      if ($response->isRedirect()) {
        $payment->update([
          'state'             => Payment::AUTHORIZE,
          'token'             => $response->getData()['TOKEN'],
          'response_details'  => response()->json($response->getData())
        ]);

        session(['purchase'   => $purchase]);
        session(['payment'    => $payment->id]);
        session()->save();

        return redirect($response->getRedirectUrl());
      } else {

        $payment->update([
          'state'             => Payment::FAILED,
          'response_details'  => response()->json($response->getData())
        ]);

        $order->update(['payment_reference' => false]);

        return response()->json(['status' => 'error', 'message' => $response->getMessage()], 500);
      }
    }
  }

  /**
   * Complete the order and payment, send order confirmation email.
   *
   * @param Order   $order
   * @param Payment $payment
   * @param array   $data
   * @param string  $transactionReference
   *
   * @return \Illuminate\Http\RedirectResponse
   */
  private function paymentCallback(Order $order, Payment $payment = null, array $data = null, $transactionReference = null)
  {
    if ($payment) {

      $details = [];

      if (isset($data['payment_method_details'])) {
        // Update payment info with response
        // Card name isn't returned for some reason, so we retain this from form request
        $details = [
          'fingerprint'           => $data['payment_method_details']['card']['fingerprint'],
          'card_type'             => $data['payment_method_details']['card']['brand'],
          'card_expiry'           => $data['payment_method_details']['card']['exp_month'] . '/' . $data['payment_method_details']['card']['exp_year'],
          'card_last_digits'      => $data['payment_method_details']['card']['last4']
        ];
      }

      $payment->update([
        'state'                 => Payment::COMPLETE,
        'transaction_reference' => $transactionReference,
        'response_details'      => $payment->response_details . ' ' . response()->json($data),
      ] + $details);
    }

    $order->update([
      'state'             => Order::COMPLETE,
      'completed_at'      => Carbon::now(),
      'payment_reference' => $transactionReference
    ]);

    Mail::to($order->email)->send(new OrderComplete($order));
    if (!empty(env('EMAIL_ENQUIRY'))) {
      Mail::to(env('EMAIL_ENQUIRY'))->send(new OrderPlaced($order));
    }

    session(['token' => null]);

    return redirect('/order/' . $order->token . '/complete');
  }


  /**
   * Get stripe gateway.
   *
   * @return \Omnipay\PayPal\ExpressGateway
   */
  private function setupPaymentGateway($method)
  {
    if ($method === Payment::CREDIT_CARD) {
      $gateway = Omnipay::create(config('services.stripe.gateway'));
      return $gateway->setApiKey(config('services.stripe.secret'));
    }

    if ($method === Payment::PAYPAL) {
      $gateway = Omnipay::create(config('services.paypal.gateway'));
      $gateway->setUsername(config('services.paypal.username'));
      $gateway->setPassword(config('services.paypal.password'));
      $gateway->setSignature(config('services.paypal.signature'));
      if (!app()->environment('production')) {
        $gateway->setTestMode(true);
      }
      return $gateway;
    }
  }

  /**
   * Confirm Paypal Payment
   *
   * @param Request $request
   *
   * @return \Illuminate\Http\Response
   */
  public function confirmPayment(Request $request)
  {
    $order = Order::where('token', session('token'))->first();

    if (!$payment = Payment::whereId(session('payment'))->where('token', $request->get('token'))->first()) {
      return redirect()->route('checkout.payment');
    }

    $purchase = session('purchase');
    $purchase['token'] = $request->get('token');
    $purchase['PayerID'] = $request->get('PayerID');
    session(['purchase' => $purchase]);
    session()->save();

    $gateway = $this->setupPaymentGateway(Payment::PAYPAL);

    try {
      $response = $gateway->fetchCheckout($purchase)->send();
    } catch (\Exception $e) {
      $payment->update([
        'state'             => Payment::FAILED,
        'response_details'  => $payment->response_details . ' ' . response()->json($e->getMessage()),
      ]);

      $order->update(['payment_reference' => false]);

      return redirect()->route('checkout.payment')->with(['error' => trans('messages.payment_error')]);
    }
    if ($response->isSuccessful()) {
      $payment->update([
        'state'             => Payment::CONFIRMED,
        'payer_id'          => $purchase['PayerID'],
        'response_details'  => $payment->response_details . ' ' . response()->json($response->getData())
      ]);
      try {
        $response = $gateway->completePurchase($purchase)->send();
      } catch (\Exception $e) {
        $payment->update([
          'state' => Payment::FAILED,
          'response_details' => $payment->response_details . ' ' . response()->json($e->getMessage()),
        ]);

        $order->update(['payment_reference' => false]);

        return redirect()->route('checkout.payment')->with(['error' => trans('messages.payment_error')]);
      }

      if ($response->isSuccessful()) {
        return $this->paymentCallback($order, $payment, $response->getData(), $response->getTransactionReference());
      } else {
        $payment->update([
          'state' => Payment::FAILED,
          'response_details' => $payment->response_details . ' ' . response()->json($response->getData())
        ]);

        $order->update(['payment_reference' => false]);

        return redirect()->route('checkout.show')->with(['error' => $response->getMessage()]);
      }
    } else {
      $payment->update([
        'state'             => Payment::FAILED,
        'response_details'  => $payment->response_details . ' ' . response()->json($response->getData())
      ]);

      $order->update(['payment_reference' => false]);

      return redirect()->route('checkout.show')->with(['error' => $response->getMessage()]);
    }
  }
}
