<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\User;
use App\Models\OrderItem;
use App\Models\Variant;
use Illuminate\Http\Request;
use Validator;

class OrderController extends Controller
{

  /**
   * Create new order
   *
   * @return \App\Order
   */
  private function createOrder()
  {
    session(['token' => md5(uniqid(mt_rand(), true))]);

    $user = User::whereId(auth()->id())->first();

    $order = Order::create([
      'user_id'             => auth()->id(),
      'email'               =>  $user ? $user->email : null,
      'state'               => 'cart',
      'completed_at'        => null,
      'token'               => session('token'),
      'shipping_address_id' => $user ? $user->shipping_address_id : null,
      'billing_address_id'  => $user ? $user->billing_address_id : null
    ]);

    return $order;
  }

  /**
   * Get our current order
   *
   * @return \App\Order | null
   */
  public function getOrder()
  {
    return Order::where('token', session('token'))->first();
  }


  /**
   * Display cart
   *
   * @return \Illuminate\Http\Response
   */
  public function getCart()
  {
    if (!$order = $this->getOrder()) {
      return redirect()->route('products.index');
    }

    return view('checkout.cart', compact('order'));
  }

  /**
   * Update cart
   *
   * @param Request $request
   * @return \Illuminate\Http\Response
   */
  public function updateCart(Request $request)
  {
    if (!$order = $this->getOrder()) {
      return redirect()->route('products.index');
    }

    foreach ($request->get('qty') as $id => $qty) {
      $item = OrderItem::where('order_id', $order->id)->where('variant_id', $id)->first();
      if ($qty > 0) {
        $item->update([
          'quantity' => $qty,
          'price'   =>  $item->variant->variantPrice,
          'total'   =>  $qty * $item->variant->variantPrice
        ]);
      } else {
        $item->delete();
      }
    }

    $order->update([
      'item_total'  => $order->items->sum('total'),
      'subtotal'    => $order->items->sum('total'), // TODO: item_total - discount
      'tax'         => $order->items->sum('total') * (0.11), // TODO: use rate in sitesettings
      'total'       => $order->items->sum('total') + $order->shipping_total
    ]);

    return redirect()->route('cart.show')->with('success', "Your cart has been updated!");
  }

  /**
   * Add item to cart
   *
   * @param int $id
   * @return \Illuminate\Http\Response
   */
  public function addItem(Request $request)
  {
    if (!$order = $this->getOrder()) {
      $order = $this->createOrder();
    }

    $validator = Validator::make($request->all(), [
      'variant'     => 'required',
      'quantity'    => 'required|min:1'
    ]);

    if ($validator->fails()) {
      return response()->json(['errors' => $validator->messages()], 400);
    }

    if (!$variant = Variant::whereId($request->variant)->first()) {
      return response()->json([], 400); // TODO: return something useful here
    }

    $item = $order->items->where('variant_id', $request->variant)->first();

    if ($item) {
      $qty = $item->quantity + $request->quantity;
      $item->update([
        'quantity'  => $item->quantity + $request->quantity,
        'price'     => $variant->variantPrice,
        'total'     => $qty * $item->variant->variantPrice
      ]);
    } else {
      $item = OrderItem::create([
        'variant_id'  => $request->variant,
        'order_id'    => $order->id,
        'quantity'    => $request->quantity,
        'details'  => [
          'product'   => $variant->product->name,
          'variant'   => $variant->name ? $item->variant->name : '',
          'sku'       => $variant->sku
        ],
        'price'       => $variant->variantPrice,
        'total'       => $request->quantity * $variant->variantPrice
      ]);
    }

    $cart_item = [
      'id'        => $item->variant->id,
      'name'      => $item->variant->product->name,
      'summary'   => $item->variant->summary,
      'quantity'  => $item->quantity,
      'total'     => $item->total
    ];

    return response()->json(['success' => true, 'item' =>  $cart_item], 200);
  }

  /**
   * Remove item from cart
   *
   * @param Request $request
   * @return \Illuminate\Http\Response
   */
  public function removeItem(Request $request)
  {
    $order = $this->getOrder();

    if ($order) {
      OrderItem::where('order_id', $order->id)->where('variant_id', $request->variant)->delete();
    }

    return response()->json(['success' => true], 200);
  }

  /**
   * Reduce quantity of item by one
   *
   * @param Request $request
   * @return \Illuminate\Http\Response
   */
  public function subtractQuantity(Request $request)
  {
    $order = $this->getOrder();

    if ($order && $item = OrderItem::where('order_id', $order->id)->where('variant_id', $request->variant_id)->first()) {
      $item->update(['quantity' => $item->quantity - 1]);
      if ($item->quantity < 1)
        $item->delete();
    }

    return response()->json(['success' => true, 'quantity' => $item->quantity], 200);
  }

  /**
   * Increment quantity of item by one
   *
   * @param Request $request
   * @return \Illuminate\Http\Response
   */
  public function addQuantity(Request $request)
  {
    $order = $this->getOrder();

    if ($order && $item = OrderItem::where('order_id', $order->id)->where('variant_id', $request->variant_id)->first()) {
      $item->update(['quantity' => $item->quantity + 1]);
    }

    return response()->json(['success' => true, 'quantity' => $item->quantity], 200);
  }

  /**
   * Completed order screen
   *
   * @return \Illuminate\Http\Response
   */
  public function complete($token)
  {
    if (!$order = Order::where('token', $token)->first()) {
      return redirect()->to('/');
    }

    return view('checkout.complete', compact('order'));
  }
}
