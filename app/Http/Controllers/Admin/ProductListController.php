<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libraries\Utils;
use App\Models\Product;
use App\Models\ProductList;
use Validator;

class ProductListController extends Controller
{
    /**
     * All product lists
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = ProductList::orderBy('order', 'ASC')->get();
        return view('admin.product_lists.index', compact('lists'));
    }

    /**
     * Create new product list
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator)->withInput();

        $list = ProductList::create(['name' => $request->name]);

        return redirect()->to('/admin/product_lists/' . $list->id . '/edit')->with('success', 'You have successfully created this product list!');
    }

    /**
     * Edit product list
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $list = ProductList::where('id', $id)->first();
        $products = Product::all();
        $colours = Utils::getColours();

        return view('admin.product_lists.edit', compact('list', 'products', 'colours'));
    }

    /**
     * Update product list
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator)->withInput();

        $list = ProductList::where('id', $id)->first();

        $list->update([
            'name' => $request->name,
            'description' => $request->description,
            'featured' => isset($request->featured),
            'button_text' => $request->button_text,
            'button_link' => $request->button_link,
            'button_colour' => isset($request->colour_select) && !empty($request->colour_select) ? $request->colour_select : (isset($request->colour_input) && !empty($request->colour_input) ? $request->colour_input : null)
        ]);

        $list->products()->sync(isset($request->products) ? $request->products : []);

        if ($request->background)
            $list->addMedia($request->background)->usingFileName($request->background->getClientOriginalName())->toMediaCollection('background');

        return redirect()->to('/admin/product_lists');
    }

    /**
     * Delete product list
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        ProductList::where('id', $id)->delete();

        return redirect()->back()->with('success', 'You have successfully deleted this product list!');
    }

    /**
     * Sort product lists
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function sort(Request $request)
    {
        foreach ($request->order as $order => $list)
            ProductList::where('id', $list)->update(['order' => ($order + 1)]);

        return response()->json(['success' => true], 200);
    }
}
