<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Coupon;
use Illuminate\Validation\Rule;
use Validator;

class CouponController extends Controller
{
    /**
     * Index of all coupons
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::paginate(50);

        return view('admin.coupons.index', compact('coupons'));
    }

    /**
     * Create new coupon
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|unique:coupons',
            'discount' => 'required',
            'sign' => 'required'
        ], [
            'code.required' => 'Please enter the code for this coupon',
            'code.unique' => 'Please enter a unique code',
            'discount.required' => 'Please enter the discount for this coupon',
            'sign.required' => 'Please choose the sign for what type of discount this is'
        ]);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator)->withInput();

        $coupon = Coupon::create([
            'code' => $request->code,
            'discount' => $request->discount,
            'sign' => $request->sign
        ]);
        return redirect()->to('/admin/coupons/' . $coupon->id . '/edit')->with('success', 'You have successfully created this coupon!');
    }

    /**
     * Edit existing coupon
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coupon = Coupon::where('id', $id)->first();

        return view('admin.coupons.edit', compact('coupon'));
    }

    /**
     * Update existing coupon
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'code' => ['required', Rule::unique('coupons')->ignore($id)],
            'discount' => 'required',
            'sign' => 'required'
        ]);

        if ($validator->fails())
            return redirect()->back()->withInput()->withErrors($validator)->withInput();

        $coupon = Coupon::where('id', $id)->first();
        $coupon->update([
            'code' => $request->code,
            'discount' => $request->discount,
            'sign' => $request->sign,
            'date_start' => $request->date_start,
            'date_expires' => $request->date_expires
        ]);

        return redirect()->to('/admin/coupons')->with('success', 'You have successfully updated this coupon!');
    }

    /**
     * Delete existing coupon
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        Coupon::where('id', $id)->delete();

        return redirect()->back()->with('success', 'You have successfully deleted this coupon!');
    }
}
