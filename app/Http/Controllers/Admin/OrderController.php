<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::when(isset(request()->email), function ($query) {
            return $query->where('email', 'LIKE', '%' . request()->email . '%');
        })->when(isset(request()->completed), function ($query) {
            return $query->where('completed_at', '>=', request()->completed);
        })->when(isset(request()->status), function ($query) {
            return $query->where('state', request()->status);
        })->orderBy('id', 'DESC')->paginate(50);

        return view('admin.orders.index', compact('orders'));
    }

    /**
     * View order
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::where('id', $id)->first();

        return view('admin.orders.show', compact('order'));
    }
}
