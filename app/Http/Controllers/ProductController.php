<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\CmsPage;

use App\Models\Product;
use App\Models\Specification;
use Illuminate\Http\Request;
use App\Search\ProductSearch;
use App\Models\Taxonomy;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Product index
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ProductSearch $search, $slug = 'shop') // TODO: set shop page slug in sitesettings
    {
        $search->term = $request->get('term');

        $search->filter = $request->except(['term', 'page']);

        $query = Product::when(!empty($request->term), function ($query) use ($request) {
            return $query->where('name', 'LIKE', '%' . $request->term . '%')
                ->orWhere('slug', 'LIKE', '%' . $request->term . '%')
                ->orWhere('introduction', 'LIKE', '%' . $request->term . '%')
                ->orWhere('description', 'LIKE', '%' . $request->term . '%');
        });


        if ($search->filter) {
            foreach ($search->filter as $filter => $value) {
                if ($value) {
                    $taxons = DB::table('taxons')
                        ->select('product_taxon.product_id as product_id')
                        ->join('taxonomies', 'taxonomies.id', '=', 'taxons.taxonomy_id')
                        ->join('product_taxon', 'product_taxon.taxon_id', '=', 'taxons.id')
                        ->where(function ($query) use ($filter, $value) {
                            $query = $query->where('taxonomies.slug', $filter)
                                ->where('taxons.slug', $value);
                        });

                    $query = $query->joinSub($taxons, $filter, function ($join) use ($filter) {
                        $join->on('id', '=', $filter . '.product_id');
                    });
                }
            }
        }

        $products = $query->orderBy('name', 'ASC')->paginate(20);

        $cmsPage = CmsPage::whereSlug($slug)->firstOrFail();

        $taxonomies = Taxonomy::isType('product')->get();

        return view('products.index', compact('products', 'taxonomies', 'cmsPage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $product = Product::whereSlug($slug)->first();

        $productTaxonomies = Taxonomy::isType('product')->get();

        $variantTaxonomies = Taxonomy::isType('variant')->get();

        $specifications = Specification::all();

        return view('products.show', compact('product', 'productTaxonomies', 'variantTaxonomies', 'specifications'));
    }
}
