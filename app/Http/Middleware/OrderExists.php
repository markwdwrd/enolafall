<?php

namespace App\Http\Middleware;

use App\Models\Order;
use Carbon\Carbon;
use Closure;

class OrderExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $order = Order::where('token', session('token'))->first();
        if (!$order || $order->item_count < 1)
            return redirect()->to('/cart');

        if ($order->coupon) {
            $coupon = $order->coupon;
            if ($coupon->date_start == null || $coupon->date_expires == null || Carbon::now() < $coupon->date_start || Carbon::now() > $coupon->date_expires)
                $order->update(['coupon_id' => null]);

            if ($coupon->products->count() > 0 && count(array_intersect($coupon->products->pluck('id')->all(), $order->items->pluck('variant.product.id')->all())) < 1)
                $order->update(['coupon_id' => null]);
        }
        return $next($request);
    }
}
