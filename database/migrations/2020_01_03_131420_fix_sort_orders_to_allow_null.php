<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixSortOrdersToAllowNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_accordion_items', function (Blueprint $table) {
            $table->integer('sort_order')->nullable()->change();
        });

        Schema::table('cms_pages', function(Blueprint $table) {
            $table->integer('sort_order')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_accordion_items', function (Blueprint $table) {
            $table->integer('sort_order')->nullable(false)->change();
        });

        Schema::table('cms_pages', function(Blueprint $table) {
            $table->integer('sort_order')->nullable(false)->change();
        });
    }
}
