<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_charges', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('shipping_charge_type_id');
            $table->unsignedBigInteger('shipping_method_id');
            $table->string('country_iso')->nullable();
            $table->unsignedBigInteger('base_charge');
            $table->unsignedBigInteger('additional_charge');
            $table->timestamps();

            $table->foreign('shipping_charge_type_id')->references('id')->on('shipping_charges')->onDelete('cascade');
            $table->foreign('shipping_method_id')->references('id')->on('shipping_methods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipping_charges', function (Blueprint $table) {
            $table->dropForeign(['shipping_charge_type_id']);
            $table->dropForeign(['shipping_method_id']);
        });

        Schema::dropIfExists('shipping_charges');
    }
}
