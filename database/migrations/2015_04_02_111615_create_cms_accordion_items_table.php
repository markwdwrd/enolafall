<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCmsAccordionItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_accordion_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('accordion_id')->nullable();
            $table->string('title');
            $table->text('content')->nullable();
            $table->unsignedBigInteger('sort_order')->default(0);
            $table->string('anchor')->nullable();
            $table->timestamps();
            $table->foreign('accordion_id')->references('id')->on('cms_accordions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_accordion_items', function (Blueprint $table) {
            $table->dropForeign(['accordion_id']);
        });

        Schema::drop('cms_accordion_items');
    }
}
