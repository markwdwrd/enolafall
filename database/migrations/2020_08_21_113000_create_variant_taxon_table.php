<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariantTaxonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variant_taxon', function (Blueprint $table) {

            $table->unsignedBigInteger('variant_id');
            $table->unsignedBigInteger('taxon_id');
            $table->unsignedBigInteger('sort_order')->default(0);

            $table->foreign('variant_id')->references('id')->on('variants')->onDelete('cascade');
            $table->foreign('taxon_id')->references('id')->on('taxons')->onDelete('cascade');

            $table->index('variant_id');
            $table->index('taxon_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('variant_taxon', function (Blueprint $table) {
            $table->dropForeign(['variant_id']);
            $table->dropForeign(['taxon_id']);
        });

        Schema::dropIfExists('variant_taxon');
    }
}
