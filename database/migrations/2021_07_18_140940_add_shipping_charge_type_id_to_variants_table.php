<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShippingChargeTypeIdToVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('variants', function (Blueprint $table) {
            $table->unsignedBigInteger('shipping_charge_type_id')->nullable()->after('product_id');
            $table->foreign('shipping_charge_type_id')->references('id')->on('shipping_charge_types')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('variants', function (Blueprint $table) {
            $table->dropForeign(['shipping_charge_type_id']);
            $table->dropColumn('shipping_charge_type_id');
        });
    }
}
