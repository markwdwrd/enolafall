<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->decimal('amount')->default(0);
            $table->unsignedBigInteger('order_id')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('state')->nullable();
            $table->string('fingerprint')->nullable();
            $table->string('transaction_reference')->nullable();
            $table->string('token')->nullable();
            $table->string('payer_id')->nullable();
            $table->text('response_details')->nullable();
            $table->string('card_type')->nullable();
            $table->string('card_name')->nullable();
            $table->string('card_expiry')->nullable();
            $table->string('card_last_digits')->nullable();

            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders');
            $table->index('order_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropForeign(['order_id']);
        });

        Schema::dropIfExists('payments');
    }
}
