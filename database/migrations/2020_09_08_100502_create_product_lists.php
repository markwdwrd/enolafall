<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductLists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_lists', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description')->nullable();
            $table->boolean('featured')->default(false);
            $table->string('button_text')->nullable();
            $table->string('button_link')->nullable();
            $table->string('button_colour')->nullable();
            $table->integer('order')->default(1);
            $table->timestamps();
        });

        Schema::create('product_list_products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('product_list_id');
            $table->unsignedBigInteger('product_id');

            $table->foreign('product_list_id')->references('id')->on('product_lists')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_list_products', function (Blueprint $table) {
            $table->dropForeign(['product_list_id']);
            $table->dropForeign(['product_id']);
        });

        Schema::dropIfExists('product_list_products');

        Schema::dropIfExists('product_lists');
    }
}
