<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogTaxonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_taxons', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('post_id');
            $table->unsignedBigInteger('taxon_id');

            $table->foreign('post_id')->references('id')->on('cms_blog_posts')->onDelete('cascade');
            $table->foreign('taxon_id')->references('id')->on('taxons')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog_taxons', function(Blueprint $table) {
            $table->dropForeign(['taxon_id']);
            $table->dropForeign(['post_id']);
        });

        Schema::dropIfExists('blog_taxons');
    }
}
