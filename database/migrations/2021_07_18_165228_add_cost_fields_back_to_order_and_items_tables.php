<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCostFieldsBackToOrderAndItemsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->decimal('item_total')->default(0)->after('shipping_address_id');
            $table->decimal('shipping_total')->nullable()->after('item_total');
            $table->decimal('subtotal')->default(0)->after('shipping_total');
            $table->decimal('tax')->nullable()->after('subtotal');
            $table->decimal('total')->default(0)->after('tax');
        });
        Schema::table('order_items', function (Blueprint $table) {
            $table->json('details')->nullable()->after('quantity');
            $table->decimal('price')->default(0)->after('details');
            $table->decimal('total')->default(0)->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('item_total');
            $table->dropColumn('shipping_total');
            $table->dropColumn('subtotal');
            $table->dropColumn('tax');
            $table->dropColumn('total');
        });
        Schema::table('order_items', function (Blueprint $table) {
            $table->dropColumn('details');
            $table->dropColumn('price');
            $table->dropColumn('total');
        });
    }
}
