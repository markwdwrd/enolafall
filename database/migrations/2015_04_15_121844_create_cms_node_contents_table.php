<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCmsNodeContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_node_contents', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cms_node_id');
            $table->unsignedBigInteger('contentable_id')->nullable();
            $table->string('contentable_type');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('cms_node_id')->references('id')->on('cms_nodes');
            $table->index('contentable_id');
            $table->index('contentable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_node_contents', function (Blueprint $table) {
            $table->dropForeign(['cms_node_id']);
        });

        Schema::drop('cms_node_contents');
    }
}
