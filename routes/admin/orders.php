<?php

use App\Http\Controllers\Admin\OrderController;

Route::group(['prefix' => 'orders', 'as' => 'orders.'], function () {
    Route::get("/", [OrderController::class, 'index'])->name('index');
    Route::get("{id}/show", [OrderController::class, 'show'])->name('show');
});
